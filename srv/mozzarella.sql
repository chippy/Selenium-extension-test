SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `mozzarella` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `mozzarella`;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` varchar(50) NOT NULL,
  `display_en` varchar(50) NOT NULL,
  `description_en` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_id` int(11) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `timestamp` date DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `extensions`;
CREATE TABLE IF NOT EXISTS `extensions` (
  `ext_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `download_link` varchar(255) NOT NULL,
  `homepage` text DEFAULT NULL,
  `support_email` varchar(255) NOT NULL,
  `contributions_url` varchar(255) NOT NULL,
  `support_url` varchar(255) NOT NULL,
  `url` text DEFAULT NULL,
  `default_locale` varchar(10) DEFAULT 'en-US',
  `lic_id` varchar(30) DEFAULT NULL,
  `created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `average_daily_users` int(11) NOT NULL DEFAULT 0,
  `weekly_downloads` int(11) NOT NULL DEFAULT 0,
  `average_rating` float NOT NULL DEFAULT 0,
  `ratings_count` int(11) NOT NULL DEFAULT 0,
  `promoted` tinyint(1) DEFAULT 0,
  `mass_surv_test_url` text DEFAULT NULL,
  `mass_surv_last_checked` datetime DEFAULT NULL,
  `mass_surv_phone_home` tinyint(1) DEFAULT 0,
  `mass_surv_phone_home_url` text DEFAULT NULL,
  `mass_surv_phone_home_url_ids` text DEFAULT NULL,
  `mass_surv_phone_home_url_bad_hosts` text DEFAULT NULL,
  `mass_surv_third_parties` tinyint(1) DEFAULT 0,
  `mass_surv_third_parties_urls` mediumtext DEFAULT NULL,
  `mass_surv_third_parties_urls_ids` text DEFAULT NULL,
  `mass_surv_third_parties_urls_bad_hosts` text DEFAULT NULL,
  `mass_surv_opens_tab_on_install` tinyint(1) DEFAULT 0,
  `mass_surv_opens_tab_on_install_urls` text DEFAULT NULL,
  `mass_surv_opens_tab_on_install_urls_ids` text DEFAULT NULL,
  `mass_surv_opens_tab_on_install_urls_bad_hosts` text DEFAULT NULL,
  `mass_surv_opens_tab_on_uninstall` tinyint(1) DEFAULT 0,
  `mass_surv_opens_tab_on_uninstall_urls` text DEFAULT NULL,
  `mass_surv_opens_tab_on_uninstall_urls_ids` text DEFAULT NULL,
  `mass_surv_opens_tab_on_uninstall_urls_bad_hosts` text DEFAULT NULL,
  `mass_surv_opens_port` tinyint(11) DEFAULT 0,
  `mass_surv_opens_port_urls` text DEFAULT NULL,
  `mass_surv_requires_manual_verification` tinyint(1) DEFAULT 0,
  `mass_surv_requires_user_click` tinyint(1) DEFAULT 0,
  `mass_surv_Element2ClickFindBy` varchar(255) DEFAULT NULL,
  `mass_surv_Element2Click` varchar(255) DEFAULT NULL,
  `mass_surv_extensionCreatesTab` tinyint(1) DEFAULT 0,
  `mass_surv_ext_install_exception` text DEFAULT NULL,
  `mass_surv_has_analytics` text NOT NULL,
  `mass_surv_has_advertisement` text NOT NULL,
  `mass_surv_is_spying` int(11) NOT NULL,
  `mass_surv_last_parsed` datetime DEFAULT NULL,
  PRIMARY KEY (`ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `extensions_fast`;
CREATE TABLE IF NOT EXISTS `extensions_fast` (
  `ext_id` int(11) NOT NULL,
  `name_ext` varchar(255) NOT NULL,
  `last_checked` datetime NOT NULL,
  `last_parsed` datetime DEFAULT NULL,
  `phone_home` int(11) NOT NULL,
  `third_parties` int(11) NOT NULL,
  `tabs_install` int(11) NOT NULL,
  `tabs_uninstall` int(11) NOT NULL,
  `sends_history` int(11) NOT NULL,
  `open_ports` int(11) NOT NULL,
  `id_detected` int(11) NOT NULL,
  `ublock_detected` int(11) NOT NULL,
  `promoted` int(11) NOT NULL,
  `average_daily_users` int(11) NOT NULL,
  `install_exception` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `extensions_gen_temp`;
CREATE TABLE IF NOT EXISTS `extensions_gen_temp` (
  `ext_id` int(11) NOT NULL,
  `name_ext` varchar(255) NOT NULL,
  `last_checked` datetime NOT NULL,
  `last_parsed` datetime DEFAULT NULL,
  `phone_home` int(11) NOT NULL,
  `third_parties` int(11) NOT NULL,
  `tabs_install` int(11) NOT NULL,
  `tabs_uninstall` int(11) NOT NULL,
  `sends_history` int(11) NOT NULL,
  `open_ports` int(11) NOT NULL,
  `id_detected` int(11) NOT NULL,
  `ublock_detected` int(11) NOT NULL,
  `promoted` int(11) NOT NULL,
  `average_daily_users` int(11) NOT NULL,
  `install_exception` text DEFAULT NULL,
  PRIMARY KEY (`ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `extension_locale`;
CREATE TABLE IF NOT EXISTS `extension_locale` (
  `ext_id` int(11) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `summary` text DEFAULT NULL,
  PRIMARY KEY (`ext_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `ext_cat`;
CREATE TABLE IF NOT EXISTS `ext_cat` (
  `ext_id` int(11) NOT NULL,
  `cat_id` varchar(50) NOT NULL,
  `app_id` varchar(20) NOT NULL,
  PRIMARY KEY (`ext_id`,`cat_id`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `licenses`;
CREATE TABLE IF NOT EXISTS `licenses` (
  `lic_id` varchar(30) NOT NULL,
  `license_url` varchar(255) NOT NULL,
  `lic_name` varchar(100) NOT NULL,
  PRIMARY KEY (`lic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `match_ids`;
CREATE TABLE IF NOT EXISTS `match_ids` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ext_id` int(11) NOT NULL,
  `matched_ids` text NOT NULL,
  `url` text DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `preview_locale`;
CREATE TABLE IF NOT EXISTS `preview_locale` (
  `img_id` int(11) NOT NULL,
  `ext_id` int(11) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `caption` text DEFAULT NULL,
  PRIMARY KEY (`img_id`,`ext_id`,`locale`),
  KEY `ext_id` (`ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


ALTER TABLE `extension_locale`
  ADD CONSTRAINT `extension_locale_ibfk_1` FOREIGN KEY (`ext_id`) REFERENCES `extensions` (`ext_id`) ON DELETE CASCADE;

ALTER TABLE `ext_cat`
  ADD CONSTRAINT `ext_cat_ibfk_1` FOREIGN KEY (`ext_id`) REFERENCES `extensions` (`ext_id`) ON DELETE CASCADE;

ALTER TABLE `preview_locale`
  ADD CONSTRAINT `preview_locale_ibfk_1` FOREIGN KEY (`ext_id`) REFERENCES `extensions` (`ext_id`) ON DELETE CASCADE;
COMMIT;

