<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *  Copyright (C) 2024 Chippy <chippy@classictetris.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#	require_once("common.php");
	// LOAD DATABASE CONNECTION INFO OR... SEE BELOW...
	if(file_exists("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php"))
	include("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php");
	$db = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);


	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


	$browse = "popular";

	$sql_count=$db->prepare("SELECT COUNT(*) FROM extensions");
	$sql_count->execute();
	$res_count=$sql_count->fetchColumn();


	// AVOID SQL INJECTION

	$sql="SELECT * FROM extensions
        LEFT JOIN extension_locale USING (ext_id)
	WHERE 1";
	$sql.=" GROUP BY ext_id";
        #$sql .= " LIMIT 300";
	#echo "<pre>".$sql."</pre>";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$popular = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
	$stmt->closeCursor();

				##############################################################################
                                ##############################################################################
                                #                      G E N E R A T E  N E W  T A B L E                    #
                                #                      #################################                    #

                                #1) create temporary table extensions_gen_temp
                                #2) Copy temp table to extensions_gen
                                $sq="DROP TABLE IF EXISTS extensions_gen_temp;
                                     CREATE TABLE `mozzarella`.`extensions_gen_temp` (
                                                        `ext_id` INT NOT NULL , 
                                                        `name_ext` VARCHAR(255) NOT NULL , 
							`last_checked` DATETIME NOT NULL , 
							`last_parsed` DATETIME NULL,
                                                        `phone_home` INT NOT NULL , 
                                                        `third_parties` INT NOT NULL , 
                                                        `tabs_install` INT NOT NULL , 
                                                        `tabs_uninstall` INT NOT NULL , 
                                                        `sends_history` INT NOT NULL , 
                                                        `open_ports` INT NOT NULL , 
                                                        `id_detected` INT NOT NULL , 
							`ublock_detected` INT NOT NULL ,
							`promoted` INT NOT NULL , 
							`average_daily_users` INT NOT NULL,
						        `install_exception` TEXT,	
                                                        PRIMARY KEY (`ext_id`)
                                                        ) ENGINE = InnoDB";

				echo $sq."\n";
				$sqq=$db->prepare($sq);
				$sqq->execute();
				$sqq->closeCursor();

?>

		
		<?php 
			foreach($popular as $l) {
				$words_analytics=['google-analytics.com','cloudinary.com','googletagmanager','pagead2.googlesyndication.com'];
				$words_tracking=['id=','ID=','challenges.cloudflare.com','ajax.googleapis.com','fonts.googleapis.com','fonts.gstatic.com'];
				$analytics=0;
				$tracking=0;
				$spying=0;
				$oports=0;
				$aaa="";

#echo "<pre>".print_r($l)."</pre>";

				if($l["mass_surv_phone_home_url"]) { 
					$phone_home_url = json_decode($l["mass_surv_phone_home_url"], true);
					if(is_array($phone_home_url)) $phone_home_url_count = count($phone_home_url);
				}else{ 
					$phone_home_url = ""; 
					$phone_home_url_count = 0;
				}
				if($l["mass_surv_phone_home_url_ids"]) {
					$phone_home_url_ids = json_decode($l["mass_surv_phone_home"], true);
					if(is_array($phone_home_url_ids)) $phone_home_url_ids_count = count($phone_home_url_ids);
				}else{
					$phone_home_url_ids_count=0;
				}
				if($l["mass_surv_phone_home_url_bad_hosts"]){
					$phone_home_url_bad_hosts = json_decode($l["mass_surv_phone_home_url_bad_hosts"], true);
					if(is_array($phone_home_url_bad_hosts) && count(array_filter(array_unique($phone_home_url_bad_hosts)))>0) $phone_home_url_bad_hosts_count = count($phone_home_url_bad_hosts);
				}else{
					$phone_home_url_bad_hosts_count = 0;
				}


			
				if($l["mass_surv_third_parties_urls"]) { 
					$third_parties_url = json_decode($l["mass_surv_third_parties_urls"], true);
					if(is_array($third_parties_url)) $third_parties_url_count = count($third_parties_url);
				}else{ 
					$third_parties_url = ""; 
					$third_parties_url_count = 0;
				}
				if($l["mass_surv_third_parties_urls_ids"]) {
					$third_parties_urls_ids = json_decode($l["mass_surv_third_parties_urls_ids"], true);
					if(is_array($third_parties_urls_ids)) $third_parties_urls_ids_count = count($third_parties_urls_ids);
				}else{
					$third_parties_urls_ids = "";
					$third_parties_urls_ids_count=0;
				}
	#echo "<pre>third parties urls ids=".print_r($third_parties_urls_ids)."</pre>";
				if($l["mass_surv_third_parties_urls_bad_hosts"]) {
					$third_parties_urls_bad_hosts=json_decode($l["mass_surv_third_parties_urls_bad_hosts"], true);
					if(is_array($third_parties_urls_bad_hosts) && count(array_filter(array_unique($third_parties_urls_bad_hosts))) >0 ) $third_parties_urls_bad_hosts_count=count($third_parties_urls_bad_hosts);
				}else{
					$third_parties_urls_bad_hosts_count = 0;
				}


				if($l["mass_surv_opens_tab_on_install_urls"]) { 
					$tabs_on_install_url = json_decode($l["mass_surv_opens_tab_on_install_urls"], true); 
					if(is_array($tabs_on_install_url)) $tabs_on_install_url_count = count($tabs_on_install_url);
				}else{ 
					$tabs_on_install_url = ""; 
					$tabs_on_install_url_count = 0;
				}
				if($l["mass_surv_opens_tab_on_install_urls_ids"]) {
					$tabs_on_install_urls_ids = json_decode($l["mass_surv_opens_tab_on_install_urls_ids"], true);
					if(is_array($tabs_on_install_urls_ids)) $tabs_on_install_urls_ids_count=count($tabs_on_install_urls_ids);
				}else{
					$tabs_on_install_urls_ids_count=0;
				}
				if($l["mass_surv_opens_tab_on_install_urls_bad_hosts"]) {
					$tabs_on_install_urls_bad_hosts = json_decode($l["mass_surv_opens_tab_on_install_urls_bad_hosts"], true);
					if(is_array($tabs_on_install_urls_bad_hosts) && count(array_filter(array_unique($tabs_on_install_urls_bad_hosts))) >0 ) $tabs_on_install_urls_bad_hosts_count=count($tabs_on_install_urls_bad_hosts);
				}else{
					$tabs_on_install_urls_bad_hosts_count=0;
				}
				
				
				if($l["mass_surv_opens_tab_on_uninstall_urls"]) { 
					$tabs_on_uninstall_url  = json_decode($l["mass_surv_opens_tab_on_uninstall_urls"], true);
					if(is_array($tabs_on_uninstall_url)) $tabs_on_uninstall_url_count = count($tabs_on_uninstall_url);
				} else {
					$tabs_on_uninstall_url = ""; 
					$tabs_on_uninstall_url_count = 0;
				}
				if($l["mass_surv_opens_tab_on_uninstall_urls_ids"]) {
                                        $tabs_on_uninstall_urls_ids = json_decode($l["mass_surv_opens_tab_on_uninstall_urls_ids"], true);
                                        if(is_array($tabs_on_uninstall_urls_ids) && count($tabs_on_uninstall_urls_ids) > 0 ) $tabs_on_uninstall_urls_ids_count=count($tabs_on_uninstall_urls_ids);
				}else{
					$tabs_on_uninstall_urls_ids_count=0;
				}
                                if($l["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"]) {
                                        $tabs_on_uninstall_urls_bad_hosts = json_decode($l["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"], true);
                                        if(is_array($tabs_on_uninstall_urls_bad_hosts) && count(array_filter(array_unique($tabs_on_uninstall_urls_bad_hosts))) >0) $tabs_on_uninstall_urls_bad_hosts_count=count($tabs_on_uninstall_urls_bad_hosts);
				}else{
					$tabs_on_uninstall_urls_bad_hosts_count=0;
				}

				$id_detected = $phone_home_url_ids_count + $third_parties_urls_ids_count + $tabs_on_install_urls_ids_count + $tabs_on_uninstall_urls_ids_count; 
			        $bad_hosts = $phone_home_url_bad_hosts_count + $third_parties_urls_bad_hosts_count + $tabs_on_install_urls_bad_hosts_count + $tabs_on_uninstall_urls_bad_hosts_count;

				if(is_array($third_parties_url)) {

					foreach($third_parties_url as $url)
					{
						foreach($words_analytics as $word){
							if(strstr($url,$word)){
								$analytics += 1;
							}
						}
					}
					foreach($third_parties_url as $url)
                                        {
                                        	foreach($words_tracking as $word){
                                                	if(strstr($url,$word)){
                                                        	$tracking += 1;
                                                       	}
                                               	}
					}
					foreach($third_parties_url as $url)
                                        {
                                               	if(isset($l["mass_surv_test_url"]) && strstr($url,$l["mass_surv_test_url"])){
                                                       	$spying += 1;
                                                       	}
					}
					foreach($third_parties_url as $url)
                                        {
                                               	if(strstr($url,"localhost:")){
                                                       	$oports += 1;
                                                }
                                        	}
				}
				
				
				
				
				
				############################################################################
				#                              I N S E R T 
				
				$ins_sql="INSERT INTO `extensions_gen_temp` (
					`ext_id`, `name_ext`, `last_checked`,`last_parsed`, `phone_home`, `third_parties`, `tabs_install`, `tabs_uninstall`, `sends_history`, `open_ports`, `id_detected`, `ublock_detected`, `promoted`,`average_daily_users`,`install_exception`
				) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
				
				$ins_ql=$db->prepare($ins_sql);
				if (is_null($l["name"])) { echo "ID=".$l["ext_id"]."\n"; }
				$ins_ql->bindParam(1, $l["ext_id"]);
				$ins_ql->bindParam(2, $l["slug"]);
				$ins_ql->bindParam(3, $l["mass_surv_last_checked"]);
				$ins_ql->bindParam(4, $l["mass_surv_last_parsed"]);
				$ins_ql->bindParam(5, $phone_home_url_count);
				$ins_ql->bindParam(6, $third_parties_url_count);
				$ins_ql->bindParam(7, $tabs_on_install_url_count);
				$ins_ql->bindParam(8, $tabs_on_uninstall_url_count);
				$ins_ql->bindParam(9, $spying);
				$ins_ql->bindParam(10, $oports);
				$ins_ql->bindParam(11, $id_detected);
				$ins_ql->bindParam(12, $bad_hosts);
				$ins_ql->bindParam(13, $l["promoted"]);
				$ins_ql->bindParam(14, $l["average_daily_users"]);
				$ins_ql->bindParam(15, $l["mass_surv_ext_install_exception"]);

				$ins_ql->execute();
				$ins_ql->closeCursor();

			}

			$q1="DROP TABLE IF EXISTS extensions_fast;";
			$q1i=$db->prepare($q1);
			$q1i->execute();
			$q1i->closeCursor();

			$q2="CREATE TABLE extensions_fast SELECT * FROM extensions_gen_temp;";
			$q2i=$db->prepare($q2);
			$q2i->execute();
			$q2i->closeCursor();
		?>
