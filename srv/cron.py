#!/usr/bin/python3

#    A scraper for addons.mozilla.org
#
#    Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


#    Documentation for the AMO API
#    https://addons-server.readthedocs.io/en/latest/topics/api/addons.html

import json
import requests
import re
import mysql.connector
import os.path
import time
#import dotenv
from dotenv import load_dotenv

AMO_URL = "https://addons.mozilla.org/api/v5/addons/search/?type=extension&page_size=50&"
SPDX_URL = "https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json"
CATEGORIES_URL = "https://addons.mozilla.org/api/v5/addons/categories/"


def fetch_json(url):
    print("-- %s" % url)
    response = requests.get(url)
    return response.json()


def fetch_page(page, parameters=""):
    data = fetch_json(f"{AMO_URL}&page={page}{parameters}")
    page_count = data["page_count"]
    return data['results']


def fetch_licenses():
    data = fetch_json(SPDX_URL)
    collection = []
    for license in data['licenses']:
        if "isFsfLibre" in license.keys() and license["isFsfLibre"]:
            collection.append({"name": license["name"], "id": license["licenseId"],
                               "url": license["seeAlso"][0]})
    return collection


def insert_categories():
    data = fetch_json(CATEGORIES_URL)
    collection = []
    for category in data:
        mysql_query("REPLACE INTO categories (cat_id, display_en, description_en) VALUES ('%s', '%s', '%s');" %
                    (category["slug"], category["name"], category["description"]))


def insert_licenses(licenses):
    for license in licenses:
        mysql_query("REPLACE INTO licenses (lic_id, license_url, lic_name) VALUES ('%s', '%s', '%s');" %
                    (license['id'], license['url'], license['name']))


def is_license_approved(license, licenses):
    for item in licenses:
        if license == item['id']:
            return True
    return False


def sanitize(string):
    string = string.replace("'", r"\'")
    string = re.sub("https://prod.outgoing.*?/http", "http", string)
    string = re.sub("https%3A//", "https://", string)
    string = re.sub("http%3A//", "http://", string)
    return string


def download_file(url, path, filename):
    if path == "previews":
        # Separate preview files into subdirectories
        path = "/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella/images/previews/%s/" % filename[0:3]
    else:
        path = "/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella/images/icons/"
    if not os.path.isdir(path):
        os.mkdir(path)
    if not os.path.isfile("%s/%s" %(path,filename)):
        print("Downloading %s" % url)
        response = requests.get(url)
        with open("%s/%s" % (path, filename), 'wb') as f:
            f.write(response.content)


def mysql_query(query):
    #print(query)
    try:
        cursor.execute(query)
        if query[0:6] == "SELECT":
            return cursor.fetchall()
        connection.commit()
    except Exception as dbexc:
        print("Mysql Error:" + str(dbexc))
        print("%%%%%%%%  QUERY  %%%%%%%%%%")
        print(query)
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%")


def process_addon(addon):
    print("Processing %s" % addon["url"])
    promoted = True if addon['promoted'] and addon['promoted']['category'] == "recommended" else False
    download_file(addon["icons"]["64"], "icons", '%s.png' % addon["id"])
    homepage = list(addon["homepage"]["url"].values())[0] if addon["homepage"] and addon["homepage"]["url"] else ""
    support_email = list(addon["support_email"].values())[0] if addon["support_email"] else ""
    contributions_url = addon["contributions_url"]["url"] if addon["contributions_url"] and addon["contributions_url"]["url"] else ""
    support_url = list(addon["support_url"]["url"].values())[0] if addon["support_url"] else ""
    url = addon["url"]
    record = mysql_query("SELECT ext_id FROM extensions WHERE ext_id = %s" % addon["id"])
    if record == []:
        mysql_query("INSERT INTO extensions \
                (ext_id, slug, guid, download_link, lic_id, default_locale, created ,last_updated, \
                homepage, support_email, contributions_url, support_url, url, \
                average_daily_users, weekly_downloads, average_rating, ratings_count, promoted) VALUES \
                (%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', \
                '%s', '%s', '%s', '%s', '%s', \
                %s, %s, %s, %s, %s);" %
                (addon["id"], addon['slug'], addon["guid"], addon["current_version"]["file"]["url"], license,
                 addon["default_locale"], addon["created"].replace("T", " ").replace("Z", ""),
                 addon["last_updated"].replace("T", " ").replace("Z", " "),
                 homepage, support_email, contributions_url[0:255], support_url, url,
                 addon["average_daily_users"],
                 addon["weekly_downloads"], addon["ratings"]["average"], addon["ratings"]["count"], promoted))
    else:
        mysql_query("UPDATE extensions set slug='%s', guid='%s', download_link='%s', lic_id='%s', default_locale='%s', last_updated='%s', \
                homepage='%s', support_email='%s', contributions_url='%s', support_url='%s', url='%s', \
                average_daily_users=%s, weekly_downloads=%s, average_rating=%s, ratings_count=%s, promoted=%s \
                WHERE ext_id=%s" %
                (addon['slug'], addon["guid"], addon["current_version"]["file"]["url"], license,
                 addon["default_locale"], 
                 addon["last_updated"].replace("T", " ").replace("Z", " "),
                 homepage, support_email, contributions_url[0:255], support_url, url,
                 addon["average_daily_users"],
                 addon["weekly_downloads"], addon["ratings"]["average"], addon["ratings"]["count"], promoted,addon["id"]) 
                )
    names = addon["name"] if addon["name"] else {}
    summaries = addon["summary"] if addon["summary"] else {}
    descriptions = addon["description"] if addon["description"] else {}
    locales = set(names).union(set(descriptions).union(set(summaries)))
    for locale in locales:
        name = sanitize(names[locale]) if locale in names.keys() and names[locale] != '' else sanitize(addon["name"][addon["default_locale"]])
        if addon["default_locale"] in summaries.keys():
            summary = sanitize(summaries[locale]) if locale in summaries.keys() and summaries[locale] != '' else sanitize(addon["summary"][addon["default_locale"]])
        else:
            summary = ""
        if addon['description'] and addon["default_locale"] in addon["description"]:
            if addon["default_locale"] in descriptions.keys():
                description = sanitize(descriptions[locale]) if locale in descriptions.keys() else sanitize(addon["description"][addon["default_locale"]])
            else:
                description = ""
        else:
            description = ""
        mysql_query("REPLACE INTO extension_locale \
                    (ext_id, locale, name, description, summary) VALUES \
                    (%s, '%s', '%s', '%s', '%s');" %
                    (addon['id'], locale, name, description, summary))

    #for application in addon["categories"]:
    for category in addon["categories"]:
#            print("---> category in categories = " + category)
        mysql_query("REPLACE INTO ext_cat \
                        (ext_id, cat_id, app_id) VALUES \
                        (%s, '%s', '%s');" %
                        (addon["id"], category, category))
    for preview in addon["previews"]:
        #download_file(preview["image_url"], 'previews', '%s.png' % preview["id"])
        download_file(preview["thumbnail_url"], 'previews', '%s_thumb.png' % preview["id"])
        if preview["caption"]:
            for locale in preview["caption"]:
                mysql_query("REPLACE INTO preview_locale \
                            (img_id, ext_id, locale, caption) VALUES \
                            (%s, %s, '%s', '%s');" %
                            (preview["id"], addon["id"], locale, sanitize(preview["caption"][locale])))
    # TODO:
    # Mark existing extensions as deleted when API updates indicate so
    # Delete preview files and DB entries if the are no longer referenced by an extension


connection = mysql.connector.connect(host=os.environ['MYSQL_HOST'], database=os.environ['MYSQL_DB'], user=os.environ['MYSQL_USER'], password=os.environ['MYSQL_PASS'])

cursor = connection.cursor()
cursor.execute('SET NAMES utf8mb4')
cursor.execute("SET CHARACTER SET utf8mb4")
cursor.execute("SET character_set_connection=utf8mb4")

licenses = fetch_licenses()
insert_licenses(licenses)
insert_categories()
for path in ["images", "images/previews/", "images/icons/"]:
    if not os.path.isdir(path):
        os.mkdir(path)

page = 0
page_count = 600
last_process = 0

while True:
    # Runs continuously, fetching one page from the API and processing it,
    # no faster than a page every 30 seconds. Cycle back to the start continuously
    # would check all extensions every ~5 hours.
    if time.time() < last_process + 30:
        time.sleep(0)
    last_process = time.time()
    page = page + 1 if page < page_count else 1
    addons = fetch_page(page)
    for addon in addons:
        if not addon['current_version']['license']:
            continue
        license = addon['current_version']['license']['slug']
        # Filter out nonfree or unsuitable addons
        if not addon["has_eula"] and not addon["is_disabled"] and not addon["is_experimental"]\
           and not addon["requires_payment"] and addon["status"] == "public"\
           and license and is_license_approved(license, licenses):
            record = mysql_query("SELECT last_updated FROM extensions WHERE ext_id = %s" % addon["id"])
            # Process addon if missing or modified
            if record == [] or record[0][0] != addon["last_updated"]:
                process_addon(addon)
    time.sleep(20)
cursor.close()

