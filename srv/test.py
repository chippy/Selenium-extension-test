#                         MIT License
#
#       Copyright (c) 2023 Chippy[at] classictetris.net
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.




import os
import subprocess
import sys
import re
import time
import json
import urllib.request
import urllib.error
import mysql.connector
import tldextract
import psutil
import shutil
#from selenium import webdriver
from seleniumwire import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# Environment
from dotenv import load_dotenv
load_dotenv()


options = webdriver.FirefoxOptions()
options.add_argument("-headless")
options.binary_location = "/usr/local/lib/vanilla/vanilla"


def download(url,path="/tmp/file"):
    try:
        urllib.request.urlretrieve(url, path)
    except urllib.error.HTTPError as err:
        try:
            if err.code == 404:
                return "404"
            else:
                return err
        except:
            pass
    return path 
        
def sanitize(string):
    string = string.replace("'", r"\'")
    return string


def mysql_query(query):
    #print(query)
    cursor.execute(query)
    if query[0:6] == "SELECT":
        return cursor.fetchall()
    connection.commit()

def purge(dir, pattern):
    for f in os.listdir(dir):
        if re.search(pattern, f):
            if(os.path.isfile(os.path.join(dir, f))):
                try:
                    os.remove(os.path.join(dir, f))
                except Exception as e:
                    print("Impossible to remove file:"+e)

            if(os.path.isdir(os.path.join(dir, f))):
                try:
                    shutil.rmtree(os.path.join(dir, f))
                except Exception as e:
                    print("Impossible to remove fileS" + e)

testing_url="https://news.ycombinator.com/?p=" 
testing_domain=tldextract.extract(testing_url).registered_domain

connection = mysql.connector.connect(host=os.environ['MYSQL_HOST'], database=os.environ['MYSQL_DB'], user=os.environ['MYSQL_USER'], password=os.environ['MYSQL_PASS'])
cursor = connection.cursor(dictionary=True)



while True:
    sql_start = "SELECT ext_id, slug, download_link, homepage, mass_surv_requires_manual_verification, mass_surv_requires_user_click, \
                mass_surv_extensionCreatesTab \
                FROM extensions ORDER BY (mass_surv_last_checked IS NULL) DESC, mass_surv_last_checked ASC LIMIT 1"
    record = mysql_query(sql_start)
    for field in record:
        ext_id = field["ext_id"]
        slug = field["slug"]
        download_link = field["download_link"]
        homepage = field["homepage"]
        domain = tldextract.extract(homepage).registered_domain
        mass_surv_requires_manual_verification = field["mass_surv_requires_manual_verification"]
        mass_surv_requires_user_click = field["mass_surv_requires_user_click"]
        mass_surv_extensionCreatesTab = field["mass_surv_extensionCreatesTab"]
        mass_surv_last_checked = time.strftime('%Y-%m-%d %H:%M:%S')
        
        extensionRequiresUserClick = False
        mass_surv_phone_home = False
        mass_surv_third_parties = False
        mass_surv_phone_home_url = []
        mass_surv_third_parties_urls = []
        mass_surv_opens_tab_on_install = False
        mass_surv_opens_tab_on_install_urls = []
        mass_surv_requires_manual_verification = False
        mass_surv_opens_tab_on_uninstall = False
        mass_surv_opens_tab_on_uninstall_urls = []
        mass_surv_opens_port = False
        mass_surv_opens_port_urls = []

        
    driver = webdriver.Firefox(options=options)
    print("\nTesting Extension:" + slug + "\n")
    print("Downloading extension....")
    # A 404 here means that the extension was removed, we remove as well or we flag it?
    ext_path = download(download_link,"/tmp/" + slug + ".xpi")
    if (ext_path == "404"):
        # Todo: Flag this as removed or remove at once? and log this somewhere?
        # The least amount of code is the one that removes the extension from the db    
        print("The extension is not present on addons.mozilla.org, so it's been removed, we remove it as well.")
        print("The exception is: 404 File Not Found")
        mysql_query("DELETE FROM extensions where ext_id = %s" %
                (ext_id)
        )
        continue
    print("Installing " + slug + ".xpi")
    print("addon path" + ext_path)
    try:
        ext_uid = driver.install_addon(ext_path, temporary=True)
        print("Installed Extension with id: " + ext_uid)
        #time.sleep(1)
        ntabs = len(driver.window_handles)
        if ntabs > 1:
            #time.sleep(0.2)
            print("Opens tabs on install...")
            opens_tab_on_install = True
            mass_surv_opens_tab_on_install = True
            mass_surv_requires_manual_verification = True
            print("This extension opens a new tab on installing, is it asking for user input?")
            print("==========================================================================")
            print("====================!      FLAG THIS AS      VERIFY   !===================")
            print("==========================================================================")
            # We need to add a key in the item if not already there, and save the result
            # This can be parsed manually to find the few extensions that require manual input
            # BTW by doing so you can see which ones require user input and you can start wondering why
            # This too is an indication that maybe the devs wanted to avoid automated tests as much as possible
            # Or they need you to click on some sort of "ACCEPT" so they are good by the law

        while ntabs > 1:
            print("switching to second tab")
            #time.sleep(0.5)
            driver.switch_to.window(driver.window_handles[1])
            print("URL     "+driver.current_url)
            #time.sleep(0.5)
            # Add this to the db too
            mass_surv_opens_tab_on_install_urls.append(driver.current_url)
            driver.close()
            print("switch back to first tab")
            #time.sleep(0.5)
            driver.switch_to.window(driver.window_handles[0])
            ntabs = ntabs-1

        if extensionRequiresUserClick == True:
            print("This extensions requires user input...")
            if mass_surv_extensionCreatesTab == True:
                print("This extension opens a new tab...")
                print("handles:")
                print(driver.window_handles)
                print("Switching tab to tab" + driver.window_handles[1])
                driver.switch_to.window(driver.window_handles[1])
            if extensionElement2ClickFindBy == "xpath":
                #time.sleep(1)
                print("Clicking on predefined element")
                element=driver.find_element("xpath", item["extensionElement2Click"])
                WebDriverWait(driver, 20).until(EC.element_to_be_clickable((element))).click()
                
        #time.sleep(1)
        print("Open a known webiste:")
        driver.get(testing_url)
        #time.sleep(1)
        print("SNIFFING...")
        #time.sleep(1)
        for request in driver.requests:
            #if request.response:
            if (tldextract.extract(request.url).registered_domain != testing_domain):
                print(
                    request.url,
                    #request.response.status_code,
                    #request.response.headers['Content-Type']              
                )
                if tldextract.extract(request.url).registered_domain == tldextract.extract(homepage).registered_domain and tldextract.extract(request.url).registered_domain != "localhost" and tldextract.extract(request.url).registered_domain != "127.0.0.1":
                    mass_surv_phone_home = True
                    mass_surv_phone_home_url.append(sanitize(request.url))
                if tldextract.extract(request.url).registered_domain != tldextract.extract(homepage).registered_domain and tldextract.extract(request.url).registered_domain != "localhost" and tldextract.extract(request.url).registered_domain != "127.0.0.1":
                    mass_surv_third_parties = True
                    mass_surv_third_parties_urls.append(sanitize(request.url))
                if tldextract.extract(request.url).registered_domain == "localhost" or tldextract.extract(request.url).registered_domain == "127.0.0.1":
                    mass_surv_opens_port = True
                    mass_surv_opens_port_urls.append(sanitized(request.url))
                    
                    
        del driver.requests

        print("Removing Extension: " + ext_uid)
        driver.uninstall_addon(ext_uid)
        #time.sleep(0.5)
        ntabs = len(driver.window_handles)
        if ntabs > 1:
            mass_surv_opens_tab_on_uninstall = True
            print("This extension opens a new tab on UNINSTALL, is it tracking us one last time??")
            print("looping through open tabs and closing them")
            print("The extension attempted to open the following url(s)")
        while ntabs > 1:
            driver.switch_to.window(driver.window_handles[1])
            #time.sleep(0.5)
            print("URL     "+driver.current_url)
            # Add this to the db too
            mass_surv_opens_tab_on_uninstall_urls.append(driver.current_url)
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            ntabs = ntabs-1
        print("SNIFFING...")
        #time.sleep(1)
        for request in driver.requests:
            #if request.response:
            if (tldextract.extract(request.url).registered_domain != testing_domain):
                print(
                    request.url,
                    #request.response.status_code,
                    #request.response.headers['Content-Type']
                )
                if tldextract.extract(request.url).registered_domain == tldextract.extract(homepage).registered_domain and tldextract.extract(request.url).registered_domain != "localhost" and tldextract.extract(request.url).registered_domain != "127.0.0.1":
                    mass_surv_phone_home = True
                    mass_surv_phone_home_url.append(sanitize(request.url))
                if tldextract.extract(request.url).registered_domain != tldextract.extract(homepage).registered_domain and tldextract.extract(request.url).registered_domain != "localhost" and tldextract.extract(request.url).registered_domain != "127.0.0.1":
                    mass_surv_third_parties = True
                    mass_surv_third_parties_urls.append(sanitize(request.url))
                if tldextract.extract(request.url).registered_domain == "localhost" or tldextract.extract(request.url).registered_domain == "127.0.0.1":
                    mass_surv_opens_port = True
                    mass_surv_opens_port_urls.append(sanitized(request.url))

        driver.quit()
        # Update record at once?
        # and possibly replace empty array with NULL.
        if not mass_surv_phone_home_url:
            mass_surv_phone_home_url=None
        if not mass_surv_third_parties_urls:
            mass_surv_third_parties_urls=None
        if not mass_surv_opens_tab_on_install_urls:
            mass_surv_opens_tab_on_install_urls=None
        if not mass_surv_opens_tab_on_uninstall_urls:
            mass_surv_opens_tab_on_uninstall_urls=None
        if not mass_surv_opens_port_urls:
            mass_surv_opens_port_urls=None


        mysql_query("UPDATE extensions set mass_surv_test_url= '%s', mass_surv_last_checked = '%s', mass_surv_phone_home = %s, mass_surv_phone_home_url = '%s', mass_surv_third_parties = %s, mass_surv_third_parties_urls= '%s', \
                    mass_surv_opens_tab_on_install = %s, mass_surv_opens_tab_on_install_urls= '%s', mass_surv_opens_tab_on_uninstall = %s, mass_surv_opens_tab_on_uninstall_urls = '%s', mass_surv_opens_port = %s, mass_surv_opens_port_urls = '%s', mass_surv_requires_manual_verification = %s  WHERE ext_id = %s;"  %
                      (testing_url, mass_surv_last_checked, mass_surv_phone_home,  str(json.dumps(mass_surv_phone_home_url)),mass_surv_third_parties,  str(json.dumps(mass_surv_third_parties_urls)), \
                       mass_surv_opens_tab_on_install, str(json.dumps(mass_surv_opens_tab_on_install_urls)),  mass_surv_opens_tab_on_uninstall, str(json.dumps(mass_surv_opens_tab_on_uninstall_urls)), mass_surv_opens_port, str(json.dumps(mass_surv_opens_port_urls)), mass_surv_requires_manual_verification, ext_id)
                      )
        # Remove file
        print("Removing :" + ext_path)
        os.remove(ext_path)
        print("Removing temporary files")
    
        print(str(ext_uid))
        purge("/tmp",".xpi")
        purge("/tmp","Temp-")
        purge("/tmp","rust_mozprofile")
        #time.sleep(0.2)
        ## kill processes around
        #PROCNAME = "vanilla"
        #for proc in psutil.process_iter():
        #    # check whether the process name matches
        #    if proc.name() == PROCNAME:
        #        proc.kill()
        #time.sleep(3) 
        
    except Exception as e:
        # Save result somewhere
        # Break the loop  
        mysql_query("UPDATE extensions set mass_surv_last_checked = '%s', mass_surv_ext_install_exception='%s' WHERE ext_id = %s;" %
                    (mass_surv_last_checked,sanitize(str(e)), ext_id)
                    ) 
        print(e) 
        #time.sleep(2)
    # kill processes around
    PROCNAME = "vanilla"
    for proc in psutil.process_iter():
        # check whether the process name matches
        if proc.name() == PROCNAME:
            try: 
                proc.kill()
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                #print("not possible to kill process\n")
                True


    time.sleep(1)
