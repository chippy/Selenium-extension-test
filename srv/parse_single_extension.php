<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
#session_start();
#
# Check the ext_id is passed, else die
if (isset($argv) && !$argv[1]){
	die("no ext_id passed as argument\nRun the script with first ext_id as first argument");
}else{
	$ext_id=$argv[1];
}


// Download Ublock-Origin host list

function save_hostlist($hosts_url,$hosts_save){
	if (file_exists($hosts_save)){
		#$hosts_file_age=filemtime($hosts_save);
		if (filemtime($hosts_save) < time() - 86400) {
			echo "File older than one day, downloading again...\n";
			if (file_put_contents($hosts_save, file_get_contents($hosts_url)))
			{
				echo "File downloaded successfully\n";
			}
			else
			{
				echo "File downloading failed.\n";
			}

		}
		echo "File exists:\n";
	}else{
		echo "File does not exist: Downloading...\n";
		if (file_put_contents($hosts_save, file_get_contents($hosts_url)))
		{
			echo "File downloaded successfully\n";
		}
		else
		{
			echo "File downloading failed.\n";
		}

	}

}

$hosts_url="https://raw.githubusercontent.com/StevenBlack/hosts/refs/heads/master/hosts";
$hosts_save="./badhosts";


save_hostlist($hosts_url,$hosts_save);

$hosts_url="https://easylist.to/easylist/easylist.txt";
$hosts_save="./easylist";

save_hostlist($hosts_url,$hosts_save);

// LOAD DATABASE CONNECTION INFO OR... SEE BELOW...
if(file_exists("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php"))
include("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php");
$db = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);


$match_id="id=";


function get_domain($url)
{
  $pieces = parse_url($url);
  $domain = isset($pieces['host']) ? $pieces['host'] : '';
  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
    return $regs['domain'];
  }
  return false;
}



function match_it($bounch_of_urls, $match_id, $ext_id) {
	foreach ($bounch_of_urls as $url){
		if (!is_null($url) && $url !="") {
			$p_url=parse_url($url);
			if(is_null($p_url) || $p_url["scheme"]=="about" || $p_url["scheme"]=="moz-extension") {
				return null;
			}else{
				$host=$p_url["host"];
				$ret[$ext_id]["ID"][]=$ext_id;
				$ret[$ext_id]["host"][]=$host;
				$ret[$ext_id]["url"][]=$url;
				$ret[$ext_id]["domain"][]=get_domain($url);
				if(exec('grep "^0.0.0.0 '.$host.'" ./badhosts')) {
					echo "The domain".$host." is present in badhosts!\n";
					if(!is_null($url) && $url !="") 
						$ret[$ext_id]["in_bad_hosts"][]=$url;
				}else{
					echo "The domain".$host." is **NOT** present in badhosts!\n";
				}
					
				#$ret[$url]["badhosts"]=1;
				echo "\nPre-------------------url-------------\n";
				if (!is_null() && stristr($p_url["query"], $match_id)){
					echo "------------![".$url."]!-------------\n";
					if(strstr($p_url["query"], "&")){
						$puqq=explode("&",$p_url["query"]);
						foreach($puqq as $pk){
							if (strstr($pk,$match_id)){
								$ret[$ext_id]["IDS"][$url][]=$pk;
							}
							
						}
					}else{
						if(stristr($p_url["query"], $match_id)){
							$ret[$ext_id]["IDS"][$url][]=$p_url["query"];
						}
					}
				}
			}
		}
	}
	if(isset($ret) && $ret!="") return $ret;
	else return array();
}

$q="select ext_id, slug, mass_surv_test_url, 
        mass_surv_phone_home_url, 
        mass_surv_third_parties_urls, 
	mass_surv_opens_tab_on_install_urls,
	mass_surv_opens_tab_on_uninstall_urls  
        FROM 
	extensions WHERE ext_id='".$ext_id."' AND
	( mass_surv_last_parsed < date_sub(now(), interval 1 second) OR mass_surv_last_parsed IS NULL )
	AND
	mass_surv_test_url IS NOT NULL
	/*AND(
		mass_surv_phone_home=1 
		OR
		mass_surv_third_parties=1
		OR
		mass_surv_opens_tab_on_install=1
		OR
	    	mass_surv_opens_tab_on_uninstall=1
		
	)*/
	ORDER BY 
	(mass_surv_last_parsed IS NULL) DESC, mass_surv_last_parsed ASC";



$res=$db->prepare($q);
$res->execute();
$out=$res->fetchAll(PDO::FETCH_ASSOC) ?? [];
$out_count=$res->rowCount();

echo "==================\n";
echo "FOUND ".$out_count." ROWS\n";



# Look into each array of url, match the *id* and extract the value
foreach ($out as $k){
	echo $k["slug"].", ID=".$k["ext_id"]."\n";
	$phone_home_url=json_decode($k["mass_surv_phone_home_url"]);
	$third_party_url=json_decode($k["mass_surv_third_parties_urls"]);
	$inst_unist_url=json_decode($k["mass_surv_opens_tab_on_uninstall_urls"]);
	$inst_inst_url=json_decode($k["mass_surv_opens_tab_on_install_urls"]);

	#$a=array();
	
	if( is_array($phone_home_url)){
		$a = match_it($phone_home_url,$match_id,$k["ext_id"]);
	}else{
		$a=null;
	}
	echo "\nPhoneHomes:\n";
	var_dump($a);
	if (isset($a[$k["ext_id"]]["IDS"]) && !is_null($a[$k["ext_id"]]["IDS"]) || isset($a[$k["ext_id"]]["in_bad_hosts"]) && !is_null($a[$k["ext_id"]]["in_bad_hosts"])){
		if (isset($a[$k["ext_id"]]["IDS"]) && !is_null($a[$k["ext_id"]]["IDS"])) {
			$usql = "UPDATE extensions set mass_surv_phone_home_url_ids='".json_encode($a[$k["ext_id"]]["IDS"])."',";
		}else{
			$usql = "UPDATE extensions SET ";
		}
		if (!is_null($a[$k["ext_id"]]["in_bad_hosts"]) && is_array($a[$k["ext_id"]]["in_bad_hosts"])  && count(array_filter(array_unique($a[$k["ext_id"]]["in_bad_hosts"]))) >0 )
		{
			$usql.= " mass_surv_phone_home_url_bad_hosts='".json_encode(array_unique($a[$k["ext_id"]]["in_bad_hosts"]))."' ";
		}else{
			$usql.= " mass_surv_phone_home_url_bad_hosts=NULL ";
		}

	        $usql.=", mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
	}else{
		$usql="update extensions set mass_surv_phone_home_url_bad_hosts=NULL, mass_surv_phone_home_url_ids=NULL, mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
	}
	echo "SQL= ".$usql."\n";
	$updq=$db->prepare($usql);
	$updq->execute();
	
	
	
	if(is_array($third_party_url)){
		$b = match_it($third_party_url,$match_id,$k["ext_id"]);
	}else{
		$b=null;
	}
	echo "\nThirdParties:\n";
	var_dump($b);
	if (isset($b[$k["ext_id"]]["IDS"]) && !is_null($b[$k["ext_id"]]["IDS"]) || isset($b[$k["ext_id"]]["in_bad_hosts"]) && !is_null($b[$k["ext_id"]]["in_bad_hosts"])){
		if (isset($b[$k["ext_id"]]["IDS"]) && !is_null($b[$k["ext_id"]]["IDS"])){
			$usql = "UPDATE extensions set mass_surv_third_parties_urls_ids='".json_encode($b[$k["ext_id"]]["IDS"])."',";
		}else{
			$usql = "UPDATE extensions SET ";
		}
		
		if (!is_null($b[$k["ext_id"]]["in_bad_hosts"]) && is_array($b[$k["ext_id"]]["in_bad_hosts"]) && count($b[$k["ext_id"]]["in_bad_hosts"]) >0 )
                {
                        $usql.= " mass_surv_third_parties_urls_bad_hosts='".json_encode(array_unique($b[$k["ext_id"]]["in_bad_hosts"]))."' ";
		}else{
			$usql.= " mass_surv_third_parties_urls_bad_hosts=NULL ";
		}
	        $usql .= ", mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
        }else{
                $usql="update extensions set mass_surv_third_parties_urls_bad_hosts=NULL, mass_surv_third_parties_urls_ids=NULL, mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
        }
	echo "SQL= ".$usql."\n";
	$updq=$db->prepare($usql);
        $updq->execute();



	if (is_array($inst_unist_url)){
		$c = match_it($inst_unist_url,$match_id,$k["ext_id"]);
	}else{
		$c=null;
	}
	echo "\nInsUnists:\n";
	var_dump($c);
	if (isset($c[$k["ext_id"]]["IDS"]) && !is_null($c[$k["ext_id"]]["IDS"]) || isset($c[$k["ext_id"]]["in_bad_hosts"]) && !is_null($c[$k["ext_id"]]["in_bad_hosts"])){
		if (isset($c[$k["ext_id"]]["IDS"]) && !is_null($c[$k["ext_id"]]["IDS"])){
			$usql = "UPDATE extensions set mass_surv_opens_tab_on_uninstall_urls_ids='".json_encode($c[$k["ext_id"]]["IDS"])."',";
		}else{
			$usql = "UPDATE extensions SET ";
		}
		if (!is_null($c[$k["ext_id"]]["in_bad_hosts"]) && is_array($c[$k["ext_id"]]["in_bad_hosts"]) && count($c[$k["ext_id"]]["in_bad_hosts"]) >0 )
                {
                        $usql.= " mass_surv_opens_tab_on_uninstall_urls_bad_hosts='".json_encode(array_unique($c[$k["ext_id"]]["in_bad_hosts"]))."' ";
		}else{
			$usql.= " mass_surv_opens_tab_on_uninstall_urls_bad_hosts=NULL ";
		}
	        $usql.=", mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
	}else{
                $usql="update extensions set mass_surv_opens_tab_on_uninstall_urls_bad_hosts=NULL, mass_surv_opens_tab_on_uninstall_urls_ids=NULL, mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
        }
        echo "SQL= ".$usql."\n";
	$updq=$db->prepare($usql);
	$updq->execute();



	if(is_array($inst_inst_url)){
		$d = match_it($inst_inst_url,$match_id,$k["ext_id"]);
	}else{
		$d=null;
	}
        echo "\nInsIsts:\n";
        var_dump($d);
        if (isset($d[$k["ext_id"]]["IDS"]) && !is_null($d[$k["ext_id"]]["IDS"]) || isset($d[$k["ext_id"]]["in_bad_hosts"]) && !is_null($d[$k["ext_id"]]["in_bad_hosts"])){
		if (isset($d[$k["ext_id"]]["IDS"]) && !is_null($d[$k["ext_id"]]["IDS"])) {
			$usql = "UPDATE extensions set mass_surv_opens_tab_on_install_urls_ids='".json_encode($d[$k["ext_id"]]["IDS"])."',";
		}else{
			$usql = "UPDATE extensions SET ";
		}
		if (!is_null($d[$k["ext_id"]]["in_bad_hosts"]) && is_array($d[$k["ext_id"]]["in_bad_hosts"]) && count($d[$k["ext_id"]]["in_bad_hosts"]) >0 )
                {
                        $usql.= " mass_surv_opens_tab_on_install_urls_bad_hosts='".json_encode(array_unique($d[$k["ext_id"]]["in_bad_hosts"]))."' ";
		}else{
			$usql.= " mass_surv_opens_tab_on_install_urls_bad_hosts=NULL";
		}
	        $usql.=", mass_surv_last_parsed=NOW()  where ext_id='".$k["ext_id"]."'";
	}else{
                $usql="update extensions set mass_surv_opens_tab_on_install_urls_bad_hosts=NULL, mass_surv_opens_tab_on_install_urls_ids=NULL, mass_surv_last_parsed=NOW() where ext_id='".$k["ext_id"]."'";
        }
	echo "SQL= ".$usql."\n";
	$updq=$db->prepare($usql);
        $updq->execute();


	#$d=array_merge($a,$b,$c);
	#var_dump($d);
	echo "============================================+END HERE+===================================\n";
	#sleep(1);
	
}


?>
