<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *  Copyright (C) 2024 Chippy <chippy@classictetris.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#	require_once("common.php");
// LOAD DATABASE CONNECTION INFO OR... SEE BELOW...
if(file_exists("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php"))
include("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php");
$db = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


#$browse = "popular";

#$sql_count=$db->prepare("SELECT COUNT(*) FROM extensions");
#$sql_count->execute();
#$res_count=$sql_count->fetchColumn();


// AVOID SQL INJECTION

$sql="SELECT * FROM extensions";
$sql.=" WHERE mass_surv_phone_home_url_ids is not null OR mass_surv_third_parties_urls_ids is not null OR mass_surv_opens_tab_on_install_urls_ids is not null OR mass_surv_opens_tab_on_uninstall_urls_ids is not null";
#$sql.=" LIMIT 10";
#echo "<pre>".$sql."</pre>";
$stmt = $db->prepare($sql);
$stmt->execute();
$popular = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
$stmt->closeCursor();
$ccount=count($popular);
echo "\n found ".$ccount." rows \n";

##############################################################################
##############################################################################
#                      G E N E R A T E  N E W  T A B L E                    #
#                      #################################                    #

#1) create temporary table extensions_gen_temp
#2) Copy temp table to extensions_gen
$sq="DROP TABLE IF EXISTS match_ids; 
CREATE TABLE `match_ids` 
	(`ID` INT NOT NULL AUTO_INCREMENT , 
	`ext_id` INT NOT NULL , 
	`matched_ids` TEXT NOT NULL , 
	`url` TEXT,
	PRIMARY KEY (`ID`)) ENGINE = InnoDB; ";
#echo $sq."\n";
$sqq=$db->prepare($sq);
$sqq->execute();
$sqq->closeCursor();
$insd=0;
foreach($popular as $l) {
	$aaa="";
	#echo "<pre>".print_r($l)."</pre>";
	if($l["mass_surv_phone_home_url_ids"]!="") {
		echo "In IF 1\n";
		$phone_home_url_ids = json_decode($l["mass_surv_phone_home_url_ids"], true);
		if(is_array($phone_home_url_ids)) {
			echo "\tIn IF 2\n";
			$phone_home_url_ids_count = count($phone_home_url_ids);
			#echo "\tcount=".$phone_home_url_ids_count."\n";
			foreach($phone_home_url_ids as $url=>$idd) {
				echo "\t\tURL=".$url."\n";
				foreach($idd as $sid){
					$ins_sql = "INSERT INTO match_ids (ext_id, matched_ids,url) VALUES ( ?,?,?);";
					$ins_ql=$db->prepare($ins_sql);
					$ins_ql->bindParam(1, $l["ext_id"]);
					$ins_ql->bindParam(2, $sid);
					$ins_ql->bindParam(3, $url);
					$ins_ql->execute();
					$ins_ql->closeCursor();
					$insd++;
					echo "\t\t\t".$sid."\n";;
				}
			}
		}

	}else{
		$phone_home_url_ids = null;
		$phone_home_url_ids_count=0;
	}
	if($l["mass_surv_third_parties_urls_ids"]!="") {
		$third_parties_urls_ids = json_decode($l["mass_surv_third_parties_urls_ids"], true);
		if(is_array($third_parties_urls_ids)) {
			$third_parties_urls_ids_count = count($third_parties_urls_ids);
			foreach($third_parties_urls_ids as $url=>$idd) {
				foreach($idd as $sid){
					$ins_sql = "INSERT INTO match_ids (ext_id, matched_ids,url) VALUES ( ?,?,?);";
					$ins_ql=$db->prepare($ins_sql);
					$ins_ql->bindParam(1, $l["ext_id"]);
					$ins_ql->bindParam(2, $sid);
					$ins_ql->bindParam(3, $url);
					$ins_ql->execute();
					$ins_ql->closeCursor();
					$insd++;
					echo "@";
				}
			}
		}
	}else{
		$third_parties_urls_ids = null;
		$third_parties_urls_ids_count=0;
	}
	#echo "<pre>third parties urls ids=".print_r($third_parties_urls_ids)."</pre>";
	if($l["mass_surv_opens_tab_on_install_urls_ids"]) {
		$tabs_on_install_urls_ids = json_decode($l["mass_surv_opens_tab_on_install_urls_ids"], true);
		if(is_array($tabs_on_install_urls_ids)) {
			$tabs_on_install_urls_ids_count=count($tabs_on_install_urls_ids);
			foreach($tabs_on_install_urls_ids as $url=>$idd) {
				foreach($idd as $sid){
				$ins_sql = "INSERT INTO match_ids (ext_id, matched_ids,url) VALUES ( ?,?,?);";
				$ins_ql=$db->prepare($ins_sql);
				$ins_ql->bindParam(1, $l["ext_id"]);
				$ins_ql->bindParam(2, $sid);
				$ins_ql->bindParam(3, $url);
				$ins_ql->execute();
				$ins_ql->closeCursor();
				$insd++;
				echo "+";
				}
			}
		}
	}else{
		$tabs_on_install_urls_ids = null;
		$tabs_on_install_urls_ids_count=0;
	}
	if($l["mass_surv_opens_tab_on_uninstall_urls_ids"]) {
		$tabs_on_uninstall_urls_ids = json_decode($l["mass_surv_opens_tab_on_uninstall_urls_ids"], true);
		if(is_array($tabs_on_uninstall_urls_ids) && count($tabs_on_uninstall_urls_ids) > 0 ) {
			$tabs_on_uninstall_urls_ids_count=count($tabs_on_uninstall_urls_ids);
			foreach($tabs_on_uninstall_urls_ids as $url=>$idd) {
				foreach($idd as $sid){
				$ins_sql = "INSERT INTO match_ids (ext_id, matched_ids,url) VALUES ( ?,?,?);";
				$ins_ql=$db->prepare($ins_sql);
				$ins_ql->bindParam(1, $l["ext_id"]);
				$ins_ql->bindParam(2, $sid);
				$ins_ql->bindParam(3, $url);
				$ins_ql->execute();
				$ins_ql->closeCursor();
				$insd++;
				echo "-";
				}
			}
		}
	}else{
		$tabs_on_uninstall_urls_ids= null;
		$tabs_on_uninstall_urls_ids_count=0;
	}
}
echo "\n\n".$insd." Records added\n";

		?>
