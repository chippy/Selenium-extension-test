<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	// print_r($db);
	// get all categories
	$stmt = $db->prepare("SELECT DISTINCT categories.* FROM categories inner join ext_cat on categories.cat_id = ext_cat.cat_id ORDER BY cat_id");
	$stmt->execute();
	$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// print_r($categories);

	$browse = "popular";
	if(isset($_GET["browse"]) && !empty($_GET["browse"]) && in_array($_GET["browse"], ["popular", "recent"])) {
		$browse = $_GET["browse"];
	}

	$page = 1;
	if(isset($_GET["page"]) && !empty($_GET["page"]) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}
	// 30 per page

	// count the max allowed page
	$count = $db->query("SELECT count(*) c FROM extensions")->fetch(PDO::FETCH_ASSOC)["c"];

	$maxPage = ceil($count / PER_PAGE);
	
	if($maxPage && $page > $maxPage) {
		// exceeded max page
		// back to first page
		header("Location:index.php");
		die();
	}

	$offset = $page * PER_PAGE - PER_PAGE;

	// get the lastly added extensions, with his default locale name if has one (or the first one)
	// + pagination
	// display 30 per page: 30 can be displayed in 6,5,3,2 or 1 column(s) perfectly


	// get the latest, apply the page to this list if browsing the recent extensions, or apply to the most popular
	/*$stmt = $db->prepare("SELECT * FROM (
	SELECT * FROM extensions
	INNER JOIN extension_locale USING (ext_id)
	WHERE locale = :locale or locale = 'en-US'
	GROUP BY ext_id, locale
	ORDER BY ext_id DESC, FIELD(locale, :locale, 'en-US') ASC
	LIMIT :l OFFSET :o
	) AS c GROUP BY ext_id ORDER BY ext_id DESC");
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->execute();
	$latest = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
	 */
	// get popular (most installed (from this site))
	

	$sql_count=$db->prepare("SELECT COUNT(*) FROM extensions");
	$sql_count->execute();
	$res_count=$sql_count->fetchColumn();


	// AVOID SQL INJECTION
	$accepted_sort_fields=["slug", "mass_surv_last_checked", "mass_surv_phone_home", "mass_surv_third_parties"];
	$accepted_boolean_fields=["mass_surv_phone_home","mass_surv_third_parties"];
	$accepted_sort_order=["ASC","DESC"];
	if (isset($_GET["sort_on"]) && (! in_array($_GET["sort_on"], $accepted_sort_fields) || ! in_array($_GET["asc_desc"], $accepted_sort_order) )){
		print"attempting sql injection my friend?";
		die();
	}
	#echo "<pre>";
	#print_r($accepted_sort_fields);
	#echo "</pre>";

	$sql="SELECT * FROM (
	SELECT * FROM extensions
        LEFT JOIN extension_locale USING (ext_id)
	WHERE locale = :locale or locale = 'en-US'";
	if (isset($_GET["notrack"]) && $_GET["notrack"]=="true"){
        	$sql.=" AND mass_surv_phone_home=0 and mass_surv_third_parties=0 ";
	}
	$sql.=" AND mass_surv_ext_install_exception IS NULL ";

	$sql.="GROUP BY ext_id, locale";
	if(isset($_GET["sort_on"]) && isset($_GET["asc_desc"])) { 
                $sql .= " ORDER BY ".$_GET["sort_on"]. " ".$_GET["asc_desc"];
        }else{

		$sql .= " ORDER BY promoted DESC, average_daily_users DESC, FIELD(locale, :locale, 'en-US') ASC ";
	}
        $sql .= " LIMIT :l OFFSET :o
	) AS c ";
	$sql .="GROUP BY ext_id";
	if(isset($_GET["sort_on"])) { 
		$sql .= " ORDER BY ".$_GET["sort_on"];
		if(isset($_GET["asc_desc"])) {
			$sql .= " ".$_GET["asc_desc"];
		}
	}else{
		$sql .=" ORDER BY promoted DESC, average_daily_users DESC";
	}
	#echo "<pre>".$sql."</pre>";
	$stmt = $db->prepare($sql);
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->execute();
	$popular = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	<link rel="icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-180x180.png" />
	
</head>
<body>
	<?php include("includes/header.php"); ?>
	<h1 class="subtitle">Judgmental Free extensions list</h1>
	<div id="showhide" class="fluid">How do i test Extensions: <a href="https://codeberg.org/chippy/Selenium-extension-test">Codeberg Repo</a></div> 
	<?php 
		if($categories) {

			?>
				<div id="categories" class="fluid">
			<?php

			foreach($categories as $cat)
			{
				?>
					<a href="category.php?id=<?=$cat["cat_id"]?>"><?=$cat["display_en"]?></a>
				<?php
			}

			?>
				</div>
			<?php

		}
			

	?>

	<div class="fluid" id="popularTitle">
		<div id="popularTitleLeft">
			<h2>Popular</h2>
		</div>
		<div>The DB contains <?=$res_count;?> Extensions</div>

		<div class="paginationContainer">
			<?php 
			$url_append="";
			if (isset($_GET["sort_on"]) && $_GET["sort_on"] != "")
			{
				$url_append="&sort_on=".$_GET["sort_on"];
			}
			if (isset($_GET["asc_desc"]) && $_GET["asc_desc"] != "") 
			{
				$url_append .= "&asc_desc=" . $_GET["asc_desc"];
			}	
			if($page > 1) {
				?>
				<a href="index.php?page=<?=$page-1?><?=$url_append?>" class="pagination">prev</a>
				

			<?php
				}
				if($page < $maxPage) {
					?>
						<a href="index.php?page=<?=$page+1?><?=$url_append?>" class="pagination">next</a>
					<?php
				}
			?>
		</div>
	</div>
	<div id="popular" class="fluid">
	<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-color:grey;width:100%">
		<tr style="height:auto;">
		<th><small>Logo</small></th>
		<th><small><a href="?sort_on=slug&asc_desc=ASC">Name</a> <a href="?sort_on=slug&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=slug&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<!--<th><small>Desc</small></th>-->
		<th><small><a href="?sort_on=mass_surv_last_checked&asc_desc=ASC">Last Checked</a> <a href="?sort_on=mass_surv_last_checked&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=mass_surv_last_checked&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Phone<br />Home <a href="?sort_on=mass_surv_phone_home&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=mass_surv_phone_home&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Third<br />Parties <a href="?sort_on=mass_surv_third_parties&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=mass_surv_third_parties&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Opens Tabs<br />On Install</small></th>
		<th><small>Opens Tabs<br />On Uninstall</small></th>
	<!--	<th><small>Analytics</small></th>
		<th><small>Tracking</small></th>-->
		<th><small>Sends<br />History</small></th>
		<th><small>Opens<br />ports</small></th>
		<th><small>ID<br />Detected</small></th>
		<th><small>U-Block<br />Origin</small></th>
		</tr>

		
		<?php 
			foreach($popular as $l) {
				$words_analytics=['google-analytics.com','cloudinary.com','googletagmanager','pagead2.googlesyndication.com'];
				$words_tracking=['id=','ID=','challenges.cloudflare.com','ajax.googleapis.com','fonts.googleapis.com','fonts.gstatic.com'];
				$analytics=0;
				$tracking=0;
				$spying=0;
				$oports=0;
				$aaa="";

#echo "<pre>".print_r($l)."</pre>";

				if($l["mass_surv_phone_home_url"]) { 
					$phone_home_url = json_decode($l["mass_surv_phone_home_url"], true);
					if(is_array($phone_home_url)) $phone_home_url_count = count($phone_home_url);
				}else{ 
					$phone_home_url = ""; 
					$phone_home_url_count = 0;
				}
				if($l["mass_surv_phone_home_url_ids"]) {
					$phone_home_url_ids = json_decode($l["mass_surv_phone_home"], true);
					if(is_array($phone_home_url_ids)) $phone_home_url_ids_count = count($phone_home_url_ids);
				}else{
					$phone_home_url_ids_count=0;
				}
				if($l["mass_surv_phone_home_url_bad_hosts"]){
					$phone_home_url_bad_hosts = json_decode($l["mass_surv_phone_home_url_bad_hosts"], true);
					if(is_array($phone_home_url_bad_hosts)) $phone_home_url_bad_hosts_count = count($phone_home_url_bad_hosts);
				}else{
					$phone_home_url_bad_hosts_count = 0;
				}


			
				if($l["mass_surv_third_parties_urls"]) { 
					$third_parties_url = json_decode($l["mass_surv_third_parties_urls"], true);
					if(is_array($third_parties_url)) $third_parties_url_count = count($third_parties_url);
				}else{ 
					$third_parties_url = ""; 
					$third_parties_url_count = 0;
				}
				if($l["mass_surv_third_parties_urls_ids"]) {
					$third_parties_urls_ids = json_decode($l["mass_surv_third_parties_urls_ids"], true);
					if(is_array($third_parties_urls_ids)) $third_parties_urls_ids_count = count($third_parties_urls_ids);
				}else{
					$third_parties_urls_ids = "";
					$third_parties_urls_ids_count=0;
				}
	#echo "<pre>third parties urls ids=".print_r($third_parties_urls_ids)."</pre>";
				if($l["mass_surv_third_parties_urls_bad_hosts"]) {
					$third_parties_urls_bad_hosts=json_decode($l["mass_surv_third_parties_urls_bad_hosts"], true);
					if(is_array($third_parties_urls_bad_hosts)) $third_parties_urls_bad_hosts_count=count($third_parties_urls_bad_hosts);
				}else{
					$third_parties_urls_bad_hosts_count = 0;
				}


				if($l["mass_surv_opens_tab_on_install_urls"]) { 
					$tabs_on_install_url = json_decode($l["mass_surv_opens_tab_on_install_urls"], true); 
					if(is_array($tabs_on_install_url)) $tabs_on_install_url_count = count($tabs_on_install_url);
				}else{ 
					$tabs_on_install_url = ""; 
					$tabs_on_install_url_count = 0;
				}
				if($l["mass_surv_opens_tab_on_install_urls_ids"]) {
					$tabs_on_install_urls_ids = json_decode($l["mass_surv_opens_tab_on_install_urls_ids"], true);
					if(is_array($tabs_on_install_urls_ids)) $tabs_on_install_urls_ids_count=count($tabs_on_install_urls_ids);
				}else{
					$tabs_on_install_urls_ids_count=0;
				}
				if($l["mass_surv_opens_tab_on_install_urls_bad_hosts"]) {
					$tabs_on_install_urls_bad_hosts = json_decode($l["mass_surv_opens_tab_on_install_urls_bad_hosts"], true);
					if(is_array($tabs_on_install_urls_bad_hosts)) $tabs_on_install_urls_bad_hosts_count=count($tabs_on_install_urls_bad_hosts);
				}else{
					$tabs_on_install_urls_bad_hosts_count=0;
				}
				
				
				if($l["mass_surv_opens_tab_on_uninstall_urls"]) { 
					$tabs_on_uninstall_url  = json_decode($l["mass_surv_opens_tab_on_uninstall_urls"], true);
					if(is_array($tabs_on_uninstall_url)) $tabs_on_uninstall_url_count = count($tabs_on_uninstall_url);
				} else {
					$tabs_on_uninstall_url = ""; 
					$tabs_on_uninstall_url_count = 0;
				}
				if($l["mass_surv_opens_tab_on_uninstall_urls_ids"]) {
                                        $tabs_on_uninstall_urls_ids = json_decode($l["mass_surv_opens_tab_on_uninstall_urls_ids"], true);
                                        if(is_array($tabs_on_uninstall_urls_ids)) $tabs_on_uninstall_urls_ids_count=count($tabs_on_uninstall_urls_ids);
				}else{
					$tabs_on_uninstall_urls_ids_count=0;
				}
                                if($l["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"]) {
                                        $tabs_on_uninstall_urls_bad_hosts = json_decode($l["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"], true);
                                        if(is_array($tabs_on_uninstall_urls_bad_hosts)) $tabs_on_uninstall_urls_bad_hosts_count=count($tabs_on_uninstall_urls_bad_hosts);
				}else{
					$tabs_on_uninstall_urls_bad_hosts_count=0;
				}

				$id_detected = $phone_home_url_ids_count + $third_parties_urls_ids_count + $tabs_on_install_urls_ids_count + $tabs_on_uninstall_urls_ids_count; 
			        $bad_hosts = $phone_home_url_bad_hosts_count + $third_parties_urls_bad_hosts_count + $tabs_on_install_urls_bad_hosts_count + $tabs_on_uninstall_urls_bad_hosts_count;

				if(is_array($third_parties_url)) {

					foreach($third_parties_url as $url)
					{
						foreach($words_analytics as $word){
							if(strstr($url,$word)){
								$analytics += 1;
							}
						}
					}
					foreach($third_parties_url as $url)
                                        {
                                        	foreach($words_tracking as $word){
                                                	if(strstr($url,$word)){
                                                        	$tracking += 1;
                                                       	}
                                               	}
					}
					foreach($third_parties_url as $url)
                                        {
                                               	if(isset($l["mass_surv_test_url"]) && strstr($url,$l["mass_surv_test_url"])){
                                                       	$spying += 1;
                                                       	}
					}
					foreach($third_parties_url as $url)
                                        {
                                               	if(strstr($url,"localhost:")){
                                                       	$oports += 1;
                                                }
                                        	}
				}
				?>
				<tr>
				<td class="tdcenter"><a href="extension.php?id=<?=$l["ext_id"]?>" class=""><img src="images/icons/<?=$l["ext_id"]?>.png" style="width:25px;"></a></td>
				<td><p><a class="mright" href="extension.php?id=<?=$l["ext_id"]?>"><?=$l["name"]?></a></p></td>
				<td class="tdcenter"><small style="padding-left:5px;padding-right:5px;"><?=$l["mass_surv_last_checked"];?></small></td>
			 	<td <?php if($l["mass_surv_phone_home"]==1) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$phone_home_url_count;?></b> <small>reqs</small></td>
				<td <?php if($l["mass_surv_third_parties"]==1) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$third_parties_url_count;?></b> <small>reqs</small></td>
				<td <?php if($l["mass_surv_opens_tab_on_install"]==1) { echo " class=\"yellow-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$tabs_on_install_url_count;?></b></td>
				<td <?php if($l["mass_surv_opens_tab_on_uninstall"]==1) { echo " class=\"yellow-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$tabs_on_uninstall_url_count;?></b></td>
				<!--<td <?php if($analytics>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$analytics;?></b></td>
				<td <?php if($tracking>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$tracking;?></b></td>-->
				<td <?php if($spying>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$spying;?></b></td>
				<td <?php if($oports>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$oports;?></b></td>
				<td <?php if($id_detected>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$id_detected;?></b></td>
				<td <?php if($bad_hosts>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$bad_hosts;?></b></td>
				
				</tr>
				<?php
			}
		?>
	</table>
	</div>
	<div class="fluid">
	<h3>Explanation on columns</h3>
	<ul>
		<li>Last checked: When the extension was last tested and data collected.</li>
		<li>Phone-Home: If there was a connection to the URL of the Homepage of the extension. This is the amount of requests made</li>
		<li>Third Parties: If after the installation, there were connections to url different then the Homepage of the extension</li>
		<li>Open tabs on Install: If during the installation of the extensions new tabs in FF were created</li>
		<li>Open tabs on Uninstall: If during/after teh removal of the extensions new tabs in FF were created</li>
		<li>Send History: If the visited websites are sent to third parties or to the Homepage of the extension</li>
		<li>Open Ports: If the extension install and runs a network service(tcp)</li>
		<li>ID Detected: Whether during parsing, one or more connections to third parties or to the extension's Homepage contain some form of ID (can also be advertiser id)</li>
		<li>Tracking: Whether one or more of the domains contacted is present in Ublock Origin list</li>
	</ul>
	</div>
	<div class="fluid" id="popularTitle">
		<div id="popularTitleLeft">
			<h2>Popular</h2>
			<div class="paginationContainer">
	                        <?php
	                        $url_append="";
	                        if (isset($_GET["sort_on"]) && $_GET["sort_on"] != "")
	                        {
	                                $url_append="&sort_on=".$_GET["sort_on"];
	                        }
	                        if (isset($_GET["asc_desc"]) && $_GET["asc_desc"] != "")
       		                {
        	                        $url_append .= "&asc_desc=" . $_GET["asc_desc"];
        	                }
        	                if($page > 1) {
        	                        ?>
                                <a href="?page=<?=$page-1?><?=$url_append?>" class="pagination">prev</a>


                        	<?php
                                }
                                if($page < $maxPage) {
                                        ?>
                                                <a href="?page=<?=$page+1?><?=$url_append?>" class="pagination">next</a>
                                        <?php
                                }
                        	?>
                	</div>
        	</div>
	</div>


	<?php include("includes/footer.php"); ?>

</body>
</html>
