<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *  Copyright (C) 2024 Chippy <chippy@classictetris.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


	if(isset($_GET['id']) && !empty($_GET['id']) && ctype_digit($_GET['id'])) {
		$id = $_GET['id'];
	}
	else {
		echo "ID=".$_GET['id'];
		#header("Location:index.php");
		die();
	}
	
	$locale = "en-US";
	if(isset($_SESSION["lang"]) && !empty($_SESSION["lang"]))
		$locale = $_SESSION["lang"];
	$sql = "SELECT * FROM(
	SELECT * FROM extension_locale
	INNER JOIN extensions USING (ext_id)
	INNER JOIN licenses USING (lic_id)
	WHERE ext_id = :id
	AND (locale = :locale or locale = 'en-US')
	GROUP by ext_id, locale
	ORDER by FIELD(locale, :locale, 'en-US') ASC)
	AS c GROUP BY ext_id";
	$stmt=$db->prepare($sql);
	$stmt->bindValue(":locale", $locale);
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$extension = $stmt->fetch(PDO::FETCH_ASSOC);

	if(!$extension)#if it does not find a locale to display, in the current language, then display what you have
	{
		$sql = "SELECT * FROM(
        	SELECT * FROM extension_locale
       	 	INNER JOIN extensions USING (ext_id)
        	INNER JOIN licenses USING (lic_id)
       		 WHERE ext_id = :id
		/*AND (locale = :locale or locale = '%')*/
        	GROUP by ext_id /*, locale */
        	 /*ORDER by FIELD(locale, :locale, 'en-US') ASC*/ )
        	AS c GROUP BY ext_id";
        	$stmt=$db->prepare($sql);
        	#$stmt->bindValue(":locale", $locale);
        	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
        	$stmt->execute();
        	$extension = $stmt->fetch(PDO::FETCH_ASSOC);
	}


	$sql = "SELECT * FROM (
	SELECT preview_locale.*
	FROM preview_locale
	WHERE ext_id = :id
	AND (locale = :locale or locale = 'en-US')
	GROUP BY img_id, locale
	ORDER BY FIELD(locale, :locale, 'en-US') ASC)
	AS c GROUP BY img_id";
	$stmt=$db->prepare($sql);
	$stmt->bindValue(":locale", $locale);
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$previews = $stmt->fetchall(PDO::FETCH_ASSOC);

	if(!$extension)
	{
		#header("Location:index.php");
		echo "Extension ID[".$id."] not found";
		die();
	}


	// get locales list
	$stmt = $db->prepare("SELECT locale FROM extension_locale
	WHERE ext_id = :id");
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$locales = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// print_r($locales);

	function url_list($list, $str_coloring=FALSE) {
		$str="";
		$obj=json_decode($list,true);
		#var_dump($obj);
		if ($obj != null){
			$str="<ul>";
			foreach ($obj as $url) {
				$str.="<li><a href=\"".$url."\">".domain_from_url($url)."</a> <br /> [Path: " .$url."]<br /> [Params:  ".parameters_from_url($url)."]</li>";
			}
			$str.="</ul>";
		}else{
			$str="";
		}
	return $str;
	}

	function domain_from_url($url) {
		if(!is_null($url)){
		$parse = parse_url($url);
		if(isset($parse['host'])){
			return $parse['host'];
		}else{
			return false;
		}
		}else{
			return false;
		}
	}


	function parameters_from_url($url){
		$ret="";
		$parse = parse_url($url, PHP_URL_QUERY);
		#var_dump($parse);
		if(isset($parse)) {
			#$ret="<ul>";
			parse_str($parse, $params);
			foreach ($params as $k=>$v) {
				$ret.="<b>[".$k." = ".$v."]; </b>";
			}
			#$ret.="</ul>";
		} 
		#return $parse;
		return $ret;
	}
	function comments($ext_id) {
		global $db;
		$sql="SELECT * FROM comments where ext_id = :id";
		#echo "<pre>".$sql."</pre>";
		$stmt=$db->prepare($sql);
		$stmt->bindValue(":id", $ext_id);
	        $stmt->execute();
		$comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
		#print_r($comments);
		return $comments;


	}
	function add_comment($ext_id,$comment){
		return TRUE;
	}
	function edit_comment($comment_id){
		return TRUE;
	}
	function similar_ids($id,$ext_id){
		global $db;
		if(strstr($id,"=")){
			$idex=explode("=",$id);
			$id=$idex[1];
		}else{
			$id=$id;
		}
			$sql="select matched_ids, ext_id, slug from match_ids inner join extensions using (ext_id) where match_ids.matched_ids like '%".$id."' AND match_ids.ext_id != '".$ext_id."' group by ext_id ";
			$stmt=$db->prepare($sql);
			$stmt->execute();
			$ids=$stmt->fetchAll(PDO::FETCH_ASSOC);
			if (isset($ids) && is_array($ids) && count($ids)>0){
				return($ids);
			}else{
				return false;
			}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">

	<style type="text/css">
		
		#main {
			max-width: 1366px;
			margin: auto;
			padding: 10px;
			background: white;
		}
		#content{
			padding: 20px;
		}
		h2+p {
			margin-bottom: 10px;
		}
		#dlBtn {
			display: inline-flex;
			align-items: center;
			padding: 10px;
			background: blueviolet;
			color: white;
			text-decoration: none;
			margin-top: 10px;
		}
		#dlBtn img {
			margin-right: 5px;
		}
		.thumbnail {
			padding: 10px;
			max-width: 400px;
			height: fit-content;
		}
	</style>
</head>
<body>

<?php include("includes/header.php"); ?>


<?php
	$dids=0;
	####################################################################
	if (!is_null($extension["mass_surv_third_parties_urls"])){
                $third_parties_urls = json_decode($extension["mass_surv_third_parties_urls"],true);
        }else{
                $third_parties_urls = "";
        }

        if (!is_null($extension["mass_surv_opens_tab_on_install_urls"])){
                $inst_inst_urls = json_decode($extension["mass_surv_opens_tab_on_install_urls"],true);
        }else{
                $inst_inst_urls = "";
        }

        if(!is_null($extension["mass_surv_opens_tab_on_uninstall_urls"])){
                $inst_uninst_urls = json_decode($extension["mass_surv_opens_tab_on_uninstall_urls"],true);
        }else{
                $inst_uninst_urls = "";
        }

        if(!is_null($extension["mass_surv_phone_home_url"])){
                $phone_home_url = json_decode($extension["mass_surv_phone_home_url"],true);
        }else{
                $phone_home_url = "";
	}

	if(is_array($third_parties_urls)){
		foreach($third_parties_urls as $k=>$xx){
			if(isset($extension["mass_surv_test_url"]) && (strstr($xx,$extension["mass_surv_test_url"]) || strstr($xx,urlencode($extension["mass_surv_test_url"]) ))){
			$spying[]=$xx;
			}	
                }
	}

	if(is_array($inst_uninst_urls)){
                foreach($inst_uninst_urls as $k=>$xx){
                        if(isset($extension["mass_surv_test_url"]) && (strstr($xx,$extension["mass_surv_test_url"]) || strstr($xx,urlencode($extension["mass_surv_test_url"]) ))){
                        $spying[]=$xx;
                        }
                }
	}

	if(is_array($phone_home_url)){
                foreach($phone_home_url as $k=>$xx){
                        if(isset($extension["mass_surv_test_url"]) && (strstr($xx,$extension["mass_surv_test_url"]) || strstr($xx,urlencode($extension["mass_surv_test_url"]) ))){
                        $spying[]=$xx;
                        }
                }
        }


	if(is_array($third_parties_urls)){
                foreach($third_parties_urls as $k=>$xx){
                        if(isset($extension["mass_surv_test_url"]) && (strstr($xx,$extension["mass_surv_test_url"]) || strstr($xx,urlencode($extension["mass_surv_test_url"]) ))){
                        $spying[]=$xx;
                        }
                }
        }

	####################################################################
	if (!is_null($extension["mass_surv_third_parties_urls_ids"])){
		$third_parties_urls_ids = json_decode($extension["mass_surv_third_parties_urls_ids"],true);
	}else{
		$third_parties_urls_ids = "";
	}
	
	if (!is_null($extension["mass_surv_opens_tab_on_install_urls_ids"])){
		$inst_inst_urls_ids = json_decode($extension["mass_surv_opens_tab_on_install_urls_ids"],true);
	}else{
		$inst_inst_urls_ids = "";
	}

	if(!is_null($extension["mass_surv_opens_tab_on_uninstall_urls_ids"])){
		$inst_uninst_urls_ids = json_decode($extension["mass_surv_opens_tab_on_uninstall_urls_ids"],true);
	}else{
		$inst_uninst_urls_ids = "";
	}
	
	if(!is_null($extension["mass_surv_phone_home_url_ids"])){
		$phone_home_url_ids = json_decode($extension["mass_surv_phone_home_url_ids"],true);
	}else{
		$phone_home_url_ids = "";
	}

	$str_out="";
	if(is_array($third_parties_urls_ids)) 
	{ 
		$dids++;
		foreach($third_parties_urls_ids as $xx=>$vv) {
			$str_out.="<ul>";
			$str_out.="	<li>".$xx."</li>";
			$str_out.="		<ul>";
			foreach($vv as $vvv){
				$str_out.="		<li>".$vvv."</li>";
				$c=similar_ids($vvv,$extension["ext_id"]);
				if(!is_null($c) && is_array($c) ){
					$str_out.="<details><summary>Id Also present in other ".count($c)." Extensions</summary><p><ul>";
					foreach($c as $cc){
						$str_out.="<li><a href=?id=".$cc["ext_id"].">".$cc["slug"]."</a></li>";
					}
					$str_out.="<ul></p></details>";
				}
			}
			$str_out.="	</ul>";
			$str_out.="</ul>";
		}	
	}
	if(is_array($inst_inst_urls_ids)) {
		$dids++; 
		foreach($inst_inst_urls_ids as $xx=>$vv) {
                        $str_out.="<ul>";
                        $str_out.="<li>".$xx."</li>";
                        $str_out.="     <ul>";
			foreach($vv as $vvv){
				$str_out.="             <li>".$vvv."</li>";
				$c=similar_ids($vvv ,$extension["ext_id"]);
                                if(!is_null($c) && is_array($c)){
                                        $str_out.="<details><summary>Id Also present in other ".count($c)." Extensions</summary><p><ul>";
                                        foreach($c as $cc){
                                                $str_out.="<li><a href=?id=".$cc["ext_id"].">".$cc["slug"]."</a></li>";
                                        }
                                        $str_out.="<ul></p></details>";
                                }
			}
			$str_out.="    </ul>";
                        $str_out.="</ul>";
                }
	}
	if(is_array($inst_uninst_urls_ids)) { 
		$dids++; 
		foreach($inst_uninst_urls_ids as $xx=>$vv) {
                        $str_out.="<ul>";
                        $str_out.="<li>".$xx."</li>";
                        $str_out.="     <ul>";
			foreach($vv as $vvv){
                        	$str_out.="             <li>".$vvv."</li>";
				$c=similar_ids($vvv,$extension["ext_id"]);
                                if(!is_null($c) && is_array($c)){
                                        $str_out.="<details><summary>Id Also present in other ".count($c)." Extensions</summary><p><ul>";
                                        foreach($c as $cc){
                                                $str_out.="<li><a href=?id=".$cc["ext_id"].">".$cc["slug"]."</a></li>";
                                        }
                                        $str_out.="<ul></p></details>";
                                }
			}
			$str_out.="    </ul>";
                        $str_out.="</ul>";
                }	
	}
	if(is_array($phone_home_url_ids)) { 
		$dids++; 
		foreach($phone_home_url_ids as $xx=>$vv) {
                        $str_out.="<ul>";
                        $str_out.="<li>".$xx."</li>";
                        $str_out.="     <ul>";
			foreach($vv as $vvv){
				$str_out.="             <li>".$vvv."</li>";
				$c=similar_ids($vvv,$extension["ext_id"]);
                                if(!is_null($c) && is_array($c)){
                                        $str_out.="<details><summary>Id Also present in other ".count($c)." Extensions</summary><p><ul>";
                                        foreach($c as $cc){
                                                $str_out.="<li><a href=?id=".$cc["ext_id"].">".$cc["slug"]."</a></li>";
                                        }
                                        $str_out.="<ul></p></details>";
                                }
			}
			$str_out.="    </ul>";
                        $str_out.="</ul>";
                }
	}

######################################################################################################################
######################################################################################################################
	$dbad_hosts=0;
        if (!is_null($extension["mass_surv_third_parties_urls_bad_hosts"])){
                $third_parties_urls_bad_hosts = json_decode($extension["mass_surv_third_parties_urls_bad_hosts"],true);
        }else{
                $third_parties_urls_bad_hosts = "";
        }

        if (!is_null($extension["mass_surv_opens_tab_on_install_urls_bad_hosts"])){
                $inst_inst_urls_bad_hosts = json_decode($extension["mass_surv_opens_tab_on_install_urls_bad_hosts"],true);
        }else{
                $inst_inst_urls_bad_hosts = "";
        }

        if(!is_null($extension["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"])){
                $inst_uninst_urls_bad_hosts = json_decode($extension["mass_surv_opens_tab_on_uninstall_urls_bad_hosts"],true);
        }else{
                $inst_uninst_urls_bad_hosts = "";
        }

        if(!is_null($extension["mass_surv_phone_home_url_bad_hosts"])){
                $phone_home_url_bad_hosts = json_decode($extension["mass_surv_phone_home_url_bad_hosts"],true);
        }else{
                $phone_home_url_bad_hosts = "";
        }

        $b_out="";
        if(is_array($third_parties_urls_bad_hosts) && count(array_filter(array_unique($third_parties_urls_bad_hosts))) > 0 )
        {
		$dbad_hosts++;
		array_unique($third_parties_urls_bad_hosts);
		foreach($third_parties_urls_bad_hosts as $xx) {
			$bh_out[]=domain_from_url($xx);
                }
        }
        if(is_array($inst_inst_urls_bad_hosts) && count(array_filter(array_unique($inst_inst_urls_bad_hosts))) > 0) {
		$dbad_hosts++;
		array_unique($inst_inst_urls_bad_hosts);
		foreach($inst_inst_urls_bad_hosts as $xx) {
			$bh_out[]=domain_from_url($xx);
                }
        }
        if(is_array($inst_uninst_urls_bad_hosts) && count(array_filter(array_unique($inst_uninst_urls_bad_hosts))) > 0) {
		$dbad_hosts++;
		array_unique($inst_uninst_urls_bad_hosts);
		foreach($inst_uninst_urls_bad_hosts as $xx) {
			$bh_out[]=domain_from_url($xx);
                }
        }
        if(is_array($phone_home_url_bad_hosts) && count(array_filter(array_unique($phone_home_url_bad_hosts))) > 0) {
		$dbad_hosts++;
		array_unique($phone_home_url_bad_hosts);
		foreach($phone_home_url_bad_hosts as $xx) {
			$bh_out[]=domain_from_url($xx);
                }
	}

	if(isset($bh_out) && is_array($bh_out) && count(array_filter(array_unique($bh_out)))>0){
		$bh_out = array_filter(array_unique($bh_out));
		$b_out.="<ul>";
		foreach($bh_out as $bo){
                        $b_out.="<li>".$bo."</li>";
		}
                $b_out.="</ul>";
	}
	
?>
	
	<div id="main">
		<h1><?=$extension["name"]?></h1>
	<div id="content">
		<img src=images/icons/<?=$extension["ext_id"]?>.png />
		<details><summary>Description From Author</summary>
		<p class=""><?=nl2br($extension["description"]) ?? '<i>No description</i>'?></p>
		</details>
		<a id="dlBtn" href="<?=$extension["download_link"]?>"><img src="assets/install.png">Install</a>
		<ul>
			<li>License: <a href="<?=$extension["license_url"]?>" target="_blank"><?=$extension["lic_name"]?></a> </li>
			<li>Data Collected: <?=$extension["mass_surv_last_checked"]?></li>
			<li>Data Parsed: <?=$extension["mass_surv_last_parsed"]?></li>

			<li>Install Exception:<?php if(!is_null($extension["mass_surv_ext_install_exception"])) {echo "<b style=\"color:red;\">".substr($extension["mass_surv_ext_install_exception"],0,strpos($extension["mass_surv_ext_install_exception"],"Stacktrace"));}else{echo "<b style=\"color: green;\">No Error";} ?></b></li>
<?php 		if(!is_null($extension["mass_surv_ext_install_exception"])){

	}else
	{?>
			<li>Reporting Visited URLs:<?php if(isset($spying) && is_array($spying) && count($spying)>0){ echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; }?>
			<?php if (isset($spying) && is_array($spying)){?>
                                <details><summary>Requested URLs:</summary><p> Bait URL: <b>"<?=$extension["mass_surv_test_url"]?>"</b> <br /><?=url_list(json_encode($spying))?></p></details>
                        <?php } ?>
			</li>

			<li>Phone home: <?php  if ($extension["mass_surv_phone_home"]==1) { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; }?>
			<?php if ($extension["mass_surv_phone_home"]==1){?>
				<details><summary>Requested URLs:</summary><p> <?=url_list($extension["mass_surv_phone_home_url"])?></p></details>
			<?php } ?>
			</li>

			<li>Connect to third parties: <?php if($extension["mass_surv_third_parties"]==1)  { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; }?>
			<?php if ($extension["mass_surv_third_parties"]==1) {?>
				<details><summary>Requested URLs:</summary><p><?=url_list($extension["mass_surv_third_parties_urls"])?></p></details>
			<?php } ?>
			</li>

			<li>Opens new tabs on install: <?php if($extension["mass_surv_opens_tab_on_install"]==1) { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; } ?>
			<?php if ($extension["mass_surv_opens_tab_on_install"]==1) {?>
				<details><summary> Requested URLs:</summary><p><?=url_list($extension["mass_surv_opens_tab_on_install_urls"])?></p></details>
			<?php } ?>
			</li>
			

			<li>Opens new tabs on uninstall: <?php if($extension["mass_surv_opens_tab_on_uninstall_urls"] ==1) { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; }?>
			<?php if ($extension["mass_surv_opens_tab_on_uninstall_urls"]==1) {?>
				<details><summary>Requested URLs:</summary><p> <?=url_list($extension["mass_surv_opens_tab_on_uninstall_urls"])?></p></details>
			<?php } ?>
			</li>


			<li>ID Detected in requests:<?php if($dids >0) { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; }?> 
			<?php if ($dids>0){ ?>
				<details><summary>Requested URLs:</summary><p><?=$str_out;?></p></details>
			<?php } ?>
			</li>


			<li>Request domains present in Ublock-Origin:<?php if($dbad_hosts>0) { echo "<b style=\"color:red\">Yes</b>"; } else { echo "<b style=\"color:green;\">No</b>"; } ?> 
			<?php if($dbad_hosts>0){ ?>
				<details><summary>Requested Domains: </summary><p><?=$b_out;?></p></details>
			<?php }?>
			</li>

			<!--<li>Requires manual verification: <?=$extension["mass_surv_requires_manual_verification"]?></li>-->
			<li>Requires User activation: <?=$extension["mass_surv_requires_user_click"]?></li>
<?php } ?>
			<li>Weekly downloads: <?=$extension["weekly_downloads"]?></li>
			<li>Average daily users: <?=$extension["average_daily_users"]?></li>
			<li>Rating: <?=$extension["average_rating"]?>/5 of <?=$extension["ratings_count"]?> ratings</li>
			<li>Created: <?=$extension["created"]?></li>
			<li>Last updated: <?=$extension["last_updated"]?></li>
			<li>Homepage: <a href="<?=$extension["homepage"]?>"><?=$extension["homepage"]?></a></li>
			<li>Support <a href="<?=$extension["support_url"]?>">site</a> and <a href="mailto:<?=$extension["support_email"]?>">email</a></li>
			<?php if ($extension["contributions_url"]){?>
			<li><a href="<?=$extension["contributions_url"]?>">Donate</a></li>
			<?php } ?>
			<li>Orig: <a href="<?=$extension["url"]?>"><?=$extension["url"]?></a></li>
			<li>API: <a href="https://addons.mozilla.org/api/v5/addons/addon/<?=$extension["guid"]?>"><?=$extension["guid"]?></a></li>
		</ul>
	<?php if(count($previews) > 1) { ?>
	<div id="thumbnails" class="fluid">
	<?php	foreach($previews as $preview) {
		$path="images/previews/".substr($preview["img_id"],0,3)."/".$preview["img_id"]."_thumb.png";?>
		<img class="thumbnail" src="<?=$path?>" alt="<?=$preview["caption"]?>">
	<?php }	?>
	</div>
	<?php } ?>
	</div>
	<div>
	<h4>Comment from a volunteer who checked this extension in person:</h4>
	<?php
		$comments=comments($extension["ext_id"]);
		foreach($comments as $comment){
			echo $comment["comment"];
                        }
		#print_r($_POST);
	?>
	</div>
</div>
	
	<?php include("includes/footer.php"); ?>
</body>
</html>
