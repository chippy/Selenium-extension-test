<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	// print_r($db);
	// get all categories
	#$stmt = $db->prepare("SELECT DISTINCT categories.* FROM categories inner join ext_cat on categories.cat_id = ext_cat.cat_id ORDER BY cat_id");
	$stmt = $db->prepare("SELECT distinct categories.cat_id , count(ext_cat.app_id) as counter , categories.display_en FROM `categories` inner join ext_cat on categories.cat_id = ext_cat.cat_id group by categories.cat_id;");
	$stmt->execute();
	$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// print_r($categories);

	$browse = "popular";
	if(isset($_GET["browse"]) && !empty($_GET["browse"]) && in_array($_GET["browse"], ["popular", "recent"])) {
		$browse = $_GET["browse"];
	}

	$page = 1;
	if(isset($_GET["page"]) && !empty($_GET["page"]) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}
	// 30 per page

	// count the max allowed page
	$count = $db->query("SELECT count(*) c FROM extensions")->fetch(PDO::FETCH_ASSOC)["c"];

	$maxPage = ceil($count / PER_PAGE);
	
	if($maxPage && $page > $maxPage) {
		// exceeded max page
		// back to first page
		header("Location:index.php");
		die();
	}

	$offset = $page * PER_PAGE - PER_PAGE;

	// get popular (most installed (from this site))
	

	$sql_count=$db->prepare("SELECT COUNT(*) FROM extensions");
	$sql_count->execute();
	$res_count=$sql_count->fetchColumn();


	// AVOID SQL INJECTION
	$accepted_sort_fields=["name_ext", "last_checked", "average_daily_users", "last_parsed", "phone_home", "third_parties", "tabs_install", "tabs_uninstall", "sends_history", "open_ports", "id_detected", "ublock_detected"];
	$accepted_boolean_fields=["mass_surv_phone_home","mass_surv_third_parties"];
	$accepted_sort_order=["ASC","DESC"];
	if (isset($_GET["sort_on"]) && (! in_array($_GET["sort_on"], $accepted_sort_fields) || ! in_array($_GET["asc_desc"], $accepted_sort_order) )){
		print"attempting sql injection my friend?";
		die();
	}
	#echo "<pre>";
	#print_r($accepted_sort_fields);
	#echo "</pre>";

	$sql="SELECT * FROM extensions_fast";
	if(isset($_GET["sort_on"]) && isset($_GET["asc_desc"])) { 
                $sql .= " ORDER BY ".$_GET["sort_on"]. " ".$_GET["asc_desc"];
        }else{

                $sql .= " ORDER BY  average_daily_users DESC ";
	}



	$sql .= " LIMIT :l OFFSET :o ";

	#echo "<pre>".$sql."</pre>";
	$stmt = $db->prepare($sql);
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->execute();
	$popular = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	<link rel="icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon" href="https://icecatbrowser.org/assets/images/cropped-icecat-1-180x180.png" />
	
</head>
<body>
	<?php include("includes/header.php"); ?>
	<h1 class="subtitle">Judgmental Free extensions list</h1>
	<?php 
		if($categories) {

			?>
				<div id="categories" class="fluid">
			<?php

			foreach($categories as $cat)
			{
				?>
					<a href="category.php?id=<?=$cat["cat_id"]?>"><?=$cat["display_en"]." <small>(".$cat["counter"].")</small>";?></a>
				<?php
			}

			?>
				</div>
			<?php

		}
			

	?>

	<div class="fluid" id="popularTitle">
		<div id="popularTitleLeft">
			<h2>Popular</h2>
		</div>
		<div>The DB contains <?=$res_count;?> Extensions</div>

		<div class="paginationContainer">
			<?php 
			$url_append="";
			if (isset($_GET["sort_on"]) && $_GET["sort_on"] != "")
			{
				$url_append="&sort_on=".$_GET["sort_on"];
			}
			if (isset($_GET["asc_desc"]) && $_GET["asc_desc"] != "") 
			{
				$url_append .= "&asc_desc=" . $_GET["asc_desc"];
			}	
			if($page > 1) {
				?>
				<a href="?page=<?=$page-1?><?=$url_append?>" class="pagination">prev</a>
				

			<?php
				}
				if($page < $maxPage) {
					?>
						<a href="?page=<?=$page+1?><?=$url_append?>" class="pagination">next</a>
					<?php
				}
			?>
		</div>
	</div>
	<div id="popular" class="fluid">
	<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-color:grey;width:100%">
		<tr style="height:auto;">
		<th><small>Logo</small></th>
		<th><small><a href="?sort_on=name&asc_desc=ASC">Name</a> <a href="?sort_on=name&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=name&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<!--<th><small>Desc</small></th>-->
		<th><small>Last Checked <a href="?sort_on=last_checked&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=last_checked&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Last Parsed <a href="?sort_on=last_parsed&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=last_parsed&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Phone<br />Home <a href="?sort_on=phone_home&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=phone_home&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Third<br />Parties <a href="?sort_on=third_parties&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=third_parties&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Opens Tabs<br />On Install <a href="?sort_on=tabs_install&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=tabs_install&asc_desc=DESC"><i class="arrow down"></i></a></small></th>
		<th><small>Opens Tabs<br />On Uninstalli <a href="?sort_on=tabs_uninstall&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=tabs_uninstall&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		<th><small>Sends<br />History <a href="?sort_on=sends_history&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=sends_history&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		<th><small>Opens<br />ports <a href="?sort_on=open_ports&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=open_ports&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		<th><small>ID<br />Detected <a href="?sort_on=id_detected&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=id_detected&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		<th><small>U-Block<br />Origin <a href="?sort_on=ublock_detected&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=ublock_detected&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		<th><small>Average<br />Daily Users <a href="?sort_on=average_daily_users&asc_desc=ASC"><i class="arrow up"></i></a> <a href="?sort_on=average_daily_users&asc_desc=DESC"><i class="arrow down"></i></a></small></th></small></th>
		</tr>

		
		<?php 
			foreach($popular as $l) {
				?>
				<tr>
				<td class="tdcenter"><a href="extension.php?id=<?=$l["ext_id"]?>" class=""><img src="images/icons/<?=$l["ext_id"]?>.png" style="width:25px;"></a></td>
				<td><a class="mright" href="extension.php?id=<?=$l["ext_id"]?>"><?=$l["name_ext"]?></a> <?php if(!is_null($l["install_exception"])) echo "<b style=\"color: red\">!</b>"?> </td>
				<td class="tdcenter"><small style="padding-left:2px;padding-right:2px;"><?=$l["last_checked"];?></small></td>
				<td class="tdcenter"><small style="padding-left:2px;padding-right:2px;"><?=$l["last_parsed"];?></small></td>
			 	<td <?php if($l["phone_home"]>0)      { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["phone_home"];?></b> <small>reqs</small></td>
				<td <?php if($l["third_parties"]>0)   { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["third_parties"];?></b> <small>reqs</small></td>
				<td <?php if($l["tabs_install"]>0)    { echo " class=\"yellow-gradient tdcenter\""; } else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["tabs_install"];?></b></td>
				<td <?php if($l["tabs_uninstall"]>0)  { echo " class=\"yellow-gradient tdcenter\""; } else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["tabs_uninstall"];?></b></td>
				<td <?php if($l["sends_history"]>0)   { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["sends_history"];?></b></td>
				<td <?php if($l["open_ports"]>0)      { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["open_ports"];?></b></td>
				<td <?php if($l["id_detected"]>0)     { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["id_detected"];?></b></td>
				<td <?php if($l["ublock_detected"]>0) { echo " class=\"red-gradient tdcenter\""; }    else { if(!is_null($l["install_exception"])){echo " class=\"grey-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; }} ?>><b><?=$l["ublock_detected"];?></b></td>
				<td ><center><small><?=$l["average_daily_users"];?></small></center></td>
				
				</tr>
				<?php
			}
		?>
	</table>
	</div>
	<div class="fluid">
	<h3>Explanation on columns</h3>
	<ul>
		<li>Last checked: When the extension was last tested and data collected.</li>
		<li>Last Parsed: When the collected data was last parsed.</li>
		<li>Phone-Home: If there was a connection to the URL of the Homepage of the extension. This is the amount of requests made</li>
		<li>Third Parties: If after the installation, there were connections to url different then the Homepage of the extension</li>
		<li>Open tabs on Install: If during the installation of the extensions new tabs in FF were created</li>
		<li>Open tabs on Uninstall: If during/after the removal of the extensions new tabs in FF were created</li>
		<li>Send History: If the visited websites are sent to third parties or to the Homepage of the extension via URL parameters</li>
		<li>Open Ports: If the extension install and runs a network service(tcp)</li>
		<li>ID Detected: Whether during parsing, one or more connections to third parties or to the extension's Homepage contain some form of ID (can also be advertiser id)</li>
		<li>Tracking: Whether one or more of the domains contacted is present in Ublock Origin list</li>
	</ul>
	</div>
	<div class="fluid" id="popularTitle">
		<div id="popularTitleLeft">
			<h2>Popular</h2>
			<div class="paginationContainer">
	                        <?php
	                        $url_append="";
	                        if (isset($_GET["sort_on"]) && $_GET["sort_on"] != "")
	                        {
	                                $url_append="&sort_on=".$_GET["sort_on"];
	                        }
	                        if (isset($_GET["asc_desc"]) && $_GET["asc_desc"] != "")
       		                {
        	                        $url_append .= "&asc_desc=" . $_GET["asc_desc"];
        	                }
        	                if($page > 1) {
        	                        ?>
                                <a href="?page=<?=$page-1?><?=$url_append?>" class="pagination">prev</a>


                        	<?php
                                }
                                if($page < $maxPage) {
                                        ?>
                                                <a href="?page=<?=$page+1?><?=$url_append?>" class="pagination">next</a>
                                        <?php
                                }
                        	?>
                	</div>
        	</div>
	</div>


	<?php include("includes/footer.php"); ?>

</body>
</html>
