<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	if(isset($_GET['q']) && !empty($_GET['q'])) {
		$q = $_GET['q'];
	}
	else {
		header("Location:index.php");
		die();
	}

	$page = 1;
	if(isset($_GET['page']) && !empty($_GET['page']) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}
	$offset = $page * PER_PAGE - PER_PAGE;
	

	$stmt_count = $db->prepare("SELECT * FROM (
                                        SELECT * FROM extension_locale
                                        INNER JOIN extensions_fast USING (ext_id)
                                        WHERE ext_id IN (
                                                SELECT ext_id FROM extension_locale
                                                WHERE (name LIKE :q OR description LIKE :q OR summary LIKE :q)
                                                AND (locale = :locale or locale = 'en-US')
                                        )
                                        AND (locale = :locale or locale = 'en-US')
                                GROUP by ext_id,locale
                                )
				AS c GROUP BY ext_id");
	$stmt_count->bindValue(":locale", $_SESSION['lang']);
        $stmt_count->bindValue(":q", '%'.$_GET['q'].'%');
	$stmt_count->execute();
        $count=$stmt_count->rowCount();	




	$stmt = $db->prepare("SELECT * FROM (
					SELECT * FROM extension_locale
					INNER JOIN extensions_fast USING (ext_id)
					WHERE ext_id IN (
						SELECT ext_id FROM extension_locale 
						WHERE (name LIKE :q OR description LIKE :q OR summary LIKE :q)
						AND (locale = :locale or locale = 'en-US')
					)
					AND (locale = :locale or locale = 'en-US')
				GROUP by ext_id,locale
				ORDER BY average_daily_users DESC, FIELD(locale, :locale, 'en-US') ASC
				)
				AS c GROUP BY ext_id ORDER BY average_daily_users DESC LIMIT :l OFFSET :o");
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->bindValue(":q", '%'.$_GET['q'].'%');
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
        $stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->execute();

	$extensions = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
	$popular=$extensions;
	if (! $extensions){
		header("Location:index.php");
		die();
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	<style type="text/css">
	/*	#container {
			display: flex;
			max-width: 1366px;
			margin: auto;

		}
		#left {
			background: orange;
			background: white;
			margin-right: 10px;
			padding: 10px;
			display: none;
		}
		#main {
			background: white;
			flex: 1;
			padding: 10px;
			display: flex;
			flex-direction: column;
		}
		
		

		@media screen and (max-width: 1000px)
		{
			#container {
				flex-direction: column;
			}

			background: red;
		}
		#left {
			background: orange;

		}
*/
	</style>
</head>
<body>
	<?php include("includes/header.php"); ?>

	<h1 class="subtitle">Search results for "<?=htmlspecialchars($_GET["q"])?>"</h1>

	<div id="container">
		<div id="main">
			<div class="fluid" id="popularTitle">
                                <div id="popularTitleLeft">
                                        <h2>Results</h2>
                                </div>
                                <div>
				Extensions:<?= $count ?>
                                </div>
                                <div class="paginationContainer">
<pre>
<?php
                                              #  echo "page:".$page.", ceil=".ceil($count / PER_PAGE).", per page=".PER_PAGE.", count=".$count."<br />";
?>
</pre>
                                <?php
                                        if($page > 1)
                                        {
                                        ?>
                                        <a class="pagination" href="?q=<?=$q?>&page=<?=$page-1?>">Previous</a>
                                        <?php
                                        }
                                        if($page < ceil($count / PER_PAGE))
                                        {
                                        ?>
                                        <a class="pagination" href="?q=<?=$q?>&page=<?=$page+1?>">Next</a>
                                        <?php
                                        }
                                        ?>
                                </div>
                        </div>
	
			         <div id="popular" class="fluid"> 
                                <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-color:#E8E8E8;width:100%">
                                <tr style="height:auto;">
                                <th><small>Logo</small></th>
                                <th><small>Name</small></th>
                                <!--<th><small>Desc</small></th>-->
                                <th><small>Last Checked</small></th>
                                <th><small>Phone<br />Home</small></th>
                                <th><small>Third<br />Parties</small></th>
                                <th><small>Opens Tabs<br />On Install</small></th>
                                <th><small>Opens Tabs<br />On Uninstalli </small></th>
                                <th><small>Sends<br />History</small></th>
                                <th><small>Opens<br />ports</small></th>
                                <th><small>ID<br />Detected</small></th>
                                <th><small>U-Block<br />Origin</small></th>
                                </tr>
                                <?php
                                        foreach($popular as $l) {
                                ?>
                                                <tr>
                                                        <td class="tdcenter"><a href="extension.php?id=<?=$l["ext_id"]?>" class=""><img src="images/icons/<?=$l["ext_id"]?>.png" style="width:25px;"></a></td>
                                                        <td><p><a class="mright" href="extension.php?id=<?=$l["ext_id"]?>"><?=$l["name"]?></a></p></td>
                                                        <td class="tdcenter"><small style="padding-left:5px;padding-right:5px;"><?=$l["last_checked"];?></small></td>
                                                        <td <?php if($l["phone_home"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["phone_home"];?></b> <small>reqs</small></td>
                                                        <td <?php if($l["third_parties"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["third_parties"];?></b> <small>reqs</small></td>
                                                        <td <?php if($l["tabs_install"]>0) { echo " class=\"yellow-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["tabs_install"];?></b></td>
                                                        <td <?php if($l["tabs_uninstall"]>0) { echo " class=\"yellow-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["tabs_uninstall"];?></b></td>
                                                        <td <?php if($l["sends_history"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["sends_history"];?></b></td>
                                                        <td <?php if($l["open_ports"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["open_ports"];?></b></td>
                                                        <td <?php if($l["id_detected"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["id_detected"];?></b></td>
                                                        <td <?php if($l["ublock_detected"]>0) { echo " class=\"red-gradient tdcenter\""; } else { echo " class=\"green-gradient tdcenter\""; } ?>><b><?=$l["ublock_detected"];?></b></td>
                                                </tr>
                                <?php
                                        }
                                ?>
                                </table>
                        </div>

				<div id="pagination">
					
					<?php

						if($page > 1)
						{
							?>
							<a class="pagination" href="search.php?q=<?=$_GET['q']?>&page=<?=$page-1?>">Previous</a>
							<?php
						}
						if($page < ceil($count / PER_PAGE))
						{
							?>
							<a class="pagination" href="search.php?q=<?=$_GET['q']?>&page=<?=$page+1?>">Next</a>
							<?php
						}

					?>

				</div>
				



		</div>
	</div>
	<?php include("includes/footer.php"); ?>
	
	
</body>
</html>
