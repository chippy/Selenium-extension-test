<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	 ini_set('display_errors', 1);
	 ini_set('display_startup_errors', 1);
	 error_reporting(E_ALL);
	session_start();


	// LOAD DATABASE CONNECTION INFO OR... SEE BELOW...
	if(file_exists("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php"))
		include("/mnt/volume-hel1-1/icecatbrowser.org/html/mozzarella.conf.php");


	// ...OR DEFINE THE FOLLOWING	
	// define("DBHOST", "localhost");
	// define("DBNAME", "mozzarella");
	// define("DBUSER", "mozzarella");
	// define("DBPASS", "mozzarella");


	// set the default language to english (for displaying extensions)
	if(!isset($_SESSION['lang'])) $_SESSION['lang'] = "en-US";

	// common file (connection to db, constants...)
	$db = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);

	define("PER_PAGE", 50);
?>
