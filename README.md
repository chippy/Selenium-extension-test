# This automates testing of firefox extensions to spot the ones that track users and/or steal data from them

### You can see a working copy of this at https://icecatbrowser.org/mozzarella
Specifically in the extension.php page you can see some extra fields containing information that helps identify extensions that phone-home or contact third parties.

## Is your favorite extension cheating on you?

### Always double check (including these results).

You can obtain a copy of the database in json format at https://icecatbrowser.org/mozzarella/mozzarella_json_dump.json
It is generated hourly.

I'm also in the process to write another selenium script for all the extensions that are flagged as VERIFY.
This happens for the extensions that open a new tab on install, and so it might require some user input.
This will have to be displayed locally as a volunteer should complete the test by clicking on a given button.
After the first time this can be automated as long as the tester saves a correct xpath or css selector for the button to click on.

## Install Vanilla Browser. Conditio-sine-qua-non
Vanilla is Icecat without extensions, as I needed to be sure that the extension being tested is the only one present.
I rebranded and then recompiled Icecat for this. 
And BTW I don't think you can use a normal version of Firefox for this because telemetry will be sent to Mozilla and connections will get mixed up with the connections made by the extension tested.


```
apt-get -q -y --yes build-dep firefox
cd /tmp
wget https://codeberg.org/chippy/gnuzilla/releases/download/v115.12.0esr/vanilla_115.12.0esr-1_amd64.deb
sudo dpkg -i vanilla_115.12.0esr-1_amd64.deb
```
## Selenium-extension-test Virtual Environment
```
python -m venv vmozzarella
source vmozzarella/bin/activate
pip install -r requirements.txt
```

## Database
import the mozzarella.sql in mysql
`mysql# create database mozzarella`
`mysql# grant all privileges on mozzarella.* to 'user'@'localhost' identified by 'password';`
`mysql -uroot -p mozzarella < mozzarella.sql`


## Use a  .env file t store local variables
``` 
MYSQL_HOST='localhost' 
MYSQL_DB='mozzarella' 
MYSQL_USER='mozzarella' 
MYSQL_PASS='12345678'
IMGS_PATH='./images'
JSON_OUTPUT_FILE='/path-to-file/'
```

## What does what
Assuming that (on a server) the installation of Vanilla went fine, you can start 

### cron.py 

This is a clone of cron.py on the original Mozzarella site https://gnuzilla.gnu.org/mozzarella/ and it will populate the database by scraping https://addons.mozilla.org/en-US/firefox/.
Watch out for the images folder and for the disk space required. 
In my case I modified the script in order to only save thumbnails and not full images. This makes the script a bit faster and save disk space without breaking anything.
I included a systemd file for this to be ran as service, although it needs some work to be able to catch and manage possible exceptions.


### test.py 

runs selenium tests using headless browser. For the first times I advice to run this locally and without the headless option so to be able to see if anything is wrong.

### cron_export_json.py
this script will export the table extensions, in a json format


